import sys
import numpy as np
import matplotlib.pyplot as plt

def CCW(p1, p2, p3):
    if (p3[1] - p1[1]) * (p2[0] - p1[0]) <= (p2[1] - p1[1]) * (p3[0] - p1[0]):
        return True
    return False

def GiftWrapping(S):
    plt.figure()
    index = 0
    n = len(S)
    P = [None] * n
    l = np.where(S[:, 0] == np.min(S[:, 0]))
    pointOnHull = S[l[0][0]]
    i = 0
    while True:
        P[i] = pointOnHull
        endpoint = S[0]
        for j in range(1, n):
            if (endpoint[0] == pointOnHull[0] and endpoint[1] == pointOnHull[1]) or not CCW(S[j], P[i], endpoint):
                endpoint = S[j]
        i = i + 1
        pointOnHull = endpoint
        J = np.array([P[k] for k in range(n) if P[k] is not None])
        plt.clf()
        plt.plot(J[:, 0], J[:, 1], 'b-', pickradius=5)
        plt.plot(S[:, 0], S[:, 1], ".r")
        plt.axis('on')
        plt.show(block=False)
        plt.pause(0.001)
        index += 1
        if endpoint[0] == P[0][0] and endpoint[1] == P[0][1]:
            break
    for i in range(n):
        if P[-1] is None:
            del P[-1]
    P = np.array(P)


    plt.clf()
    plt.plot(P[:, 0], P[:, 1], '-b', pickradius=5)
    plt.plot([P[-1, 0], P[0, 0]], [P[-1, 1], P[0, 1]], 'b-', pickradius=5)
    plt.plot(S[:, 0], S[:, 1], ".r")
    plt.axis('on')
    plt.show(block=False)
    plt.pause(0.001)
    return P
def main():

    P = np.array([[-2,72],[-71,13],[17,-70],[9,10],[-24,67],[73,60],[-37,-77],[54,2],[78,-15],[-41,-47],[-85,-25],[70,-28]])
    L = GiftWrapping(P)

    plt.plot(L[:, 0], L[:, 1], 'or', pickradius=5)
    plt.plot([L[-1, 0], L[0, 0]], [L[-1, 1], L[0, 1]], 'b-', pickradius=5)
    plt.plot(P[:, 0], P[:, 1], ".r")
    plt.axis('off')
    plt.show()


if __name__ == '__main__':
    main()