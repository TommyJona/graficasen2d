import numpy as np
from mpmath.ctx_mp_python import return_mpc
from scipy.spatial import ConvexHull, convex_hull_plot_2d
import math
import matplotlib.pyplot as plt
from itertools import combinations

points = np.array([[-2,72],[-71,13],[17,-70],[9,10],[-24,67],[73,60],[-37,-77],[54,2],[78,-15],[-41,-47],[-85,-25],[70,-28]])# 30 random points in 2-D
hull = ConvexHull(points)
plt.plot(points[:,0], points[:,1], 'o') #escribe los puntos en azul


for simplex in hull.simplices:
    plt.plot(points[simplex, 0], points[simplex, 1], 'r-')
plt.plot(points[hull.vertices,0], points[hull.vertices,1], 'r-', lw=2)
plt.plot(points[hull.vertices[0],0], points[hull.vertices[0],1], 'ro')
plt.show()
def dist(p1, p2):
    (x1, y1), (x2, y2) = p1, p2
    return  math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
x=[-2,-71,17,9,-24,73,-37,54,78,-41,-85,70]
y=[72,13,-70,10,67,60,-77,2,-15,-47,-25,-28]
points = list(zip(x,y))
distances = [dist(p1, p2) for p1, p2 in combinations(points, 2)]
avg_distance = sum(distances) / len(distances)
print(points)
print(distances)
print(min(distances))
print(max(distances))
xr=[-2,-37,17]
yr=[71,-77,-70]
points2=list(zip(xr,yr))
distancias2=[dist(p1, p2) for p1, p2 in combinations(points2, 2)]
print(points2)
print(distancias2+points2)
print("distancia entre ei y ei+1:", min(distancias2))
print("distancia maxima de apertura entre f y la recta ",max(distancias2))
