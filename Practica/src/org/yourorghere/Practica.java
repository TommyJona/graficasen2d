package org.yourorghere;

import com.sun.opengl.util.Animator;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import com.sun.opengl.util.GLUT;




/**
 * Practica.java <BR>
 * author: Brian Paul (converted to Java by Ron Cemer and Sven Goethel) <P>
 *
 * This version is equal to Brian Paul's version 1.2 1999/10/21
 */
public class Practica implements GLEventListener {
public static int ang=0;
public static GLU glu;
public static GLUT glut;
    public static double translate=-1.5f,traslatey=3.5f,traslatex=1.5f;
    public static float translatex=-1.5f;
    public static double rotate=1f,rotatey=1f,rotatez=1f;
    public static String vector[];
    public static float tra=0,tra1=0;
    public static void main(String[] args) {
        Frame frame = new Frame("Simple JOGL Application");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new Practica());
        frame.add(canvas);
        frame.setSize(1000, 1000);
        final Animator animator = new Animator(canvas);
        
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();
        GLUT glut=new GLUT();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, h, 1.0, 20.0);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
         glu=new GLU();
         glut=new GLUT();
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT );
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();

        // Move the "drawing cursor" around
        
        //gl.glRotatef(30, 1, 1, 1);
        
        
           
//            
//         gl.glPushMatrix();
//         
//        gl.glTranslatef(0, 0, -4f);
//        gl.glRotatef(ang, 1, 0, 0);
//           
//         
//         gl.glTranslatef(0, 0.1f, 0.05f);
//         gl.glRotatef(ang, -1, 0, 0);
//         
//        gl.glBegin(GL.GL_TRIANGLES);
//         
//            gl.glColor3f(1.0f, 0.0f, 0.0f);    // Set the current drawing color to red
//            gl.glVertex3f(0.0f, 1.0f, 0.0f);   // Top
//            gl.glColor3f(0.0f, 1.0f, 0.0f);    // Set the current drawing color to green
//            gl.glVertex3f(-1.0f, -1.0f, 0.0f); // Bottom Left
//            gl.glColor3f(0.0f, 0.0f, 1.0f);    // Set the current drawing color to blue
//            gl.glVertex3f(1.0f, -1.0f, 0.0f);  // Bottom Right
//        // Finished Drawing The Triangle
//       gl.glEnd();
//       gl.glPopMatrix();
//         gl.glPushMatrix();
//        gl.glTranslatef(0, 0, -8f);
//           
//         gl.glRotatef(ang+1, 1, 0, 0);
//         gl.glTranslatef(0, 0.1f, 0.1f);
//         gl.glRotatef(ang+1, -1, 0, 0);
//        gl.glBegin(GL.GL_TRIANGLES);
//         
//            gl.glColor3f(1.0f, 0.0f, 0.0f);    // Set the current drawing color to red
//            gl.glVertex3f(0.0f, 1.0f, 0.0f);   // Top
//            gl.glColor3f(0.0f, 1.0f, 0.0f);    // Set the current drawing color to green
//            gl.glVertex3f(-1.0f, -1.0f, 0.0f); // Bottom Left
//            gl.glColor3f(0.0f, 0.0f, 1.0f);    // Set the current drawing color to blue
//            gl.glVertex3f(1.0f, -1.0f, 0.0f);  // Bottom Right
//        // Finished Drawing The Triangle
//       gl.glEnd();
//       gl.glPopMatrix();
        
         
         

          //estrellas 
           gl.glPushMatrix();
        for (float i = 0; i < 30; i++) {
            for (float j = 0; j < 25; j++) {
                 gl.glPushMatrix();
         gl.glTranslatef(-5.3f+i/2, 4f-j/3 , -10.0f);
        gl.glScalef(0.015f, 0.015f, 0f);
        gl.glRotatef(90,1, 1, 0);
         
        gl.glColor3f(0.9f, 0.9f, 0.9f);    // Set the current drawing color to light blue
        glut.glutSolidTetrahedron();// Bottom Left
        gl.glPopMatrix();
            }
           
        }
        gl.glPopMatrix();
        
       //orbita mercurio
       gl.glPushMatrix();
         gl.glTranslatef(1f, -3.3f , -20.0f);
        gl.glScalef(1.3f, 1f, 0);
         gl.glRotatef(50,-1, 0, 0);
         gl.glBegin(gl.GL_POINTS);
            for(int i=0; i<200; i++){
               float x = (float) Math.cos(i*5*3.14/200);
               float y = (float) Math.sin(i*5*3.14/200);
               gl.glVertex3f(x, y+5,0); gl.glColor3f(1.0f, 1f, 1f);
            }
        gl.glEnd();
        gl.glPopMatrix();
        
        //mercurio
        gl.glPushMatrix();
        gl.glTranslatef((float )translate+2.5f, (float) traslatey-1.5f, -20.0f);
        gl.glScalef(0.2f, 0.2f, 0f);

        gl.glRotatef(ang,0, 1, 0.5f);
        gl.glTranslatef(6.0f, -10.0f, -7.0f);
        gl.glColor3f(0.5f, 0.5f, 1.0f);    // Set the current drawing color to light blue
        glut.glutSolidSphere(1, 20, 20);// Bottom Left
        
        gl.glPopMatrix();
        //orbita venus
        gl.glPushMatrix();
         gl.glTranslatef(1f, -5.0f , -20.0f);
        gl.glScalef(2.3f, 2f, 0);
         gl.glRotatef(60,-1, 0, 0);
         gl.glBegin(gl.GL_POINTS);
            for(int i=0; i<200; i++){
               float x = (float) Math.cos(i*5*3.14/200);
               float y = (float) Math.sin(i*5*3.14/200);
               gl.glVertex3f(x, y+5,0); gl.glColor3f(1.0f, 1f, 1f);
            }
        gl.glEnd();
        gl.glPopMatrix();
        
        
        // venus
        gl.glPushMatrix();
        gl.glTranslatef((float )translate+2.2f, (float) traslatey-4, -15.0f);
        gl.glScalef(0.25f, 0.25f, 0f);
         gl.glRotatef(ang,0, 1, -0.5f);
        gl.glTranslatef(1.0f, -1.0f, -7.0f);
        gl.glColor3f(1.5f, 0f, 1.0f);    // Set the current drawing color to light blue
        glut.glutSolidSphere(1, 20, 20);// Bottom Left
        // Done Drawing The Quad
        gl.glPopMatrix();
        
         //orbita tierra
         gl.glPushMatrix();
         gl.glTranslatef(1f, -7.6f , -20.0f);
        gl.glScalef(3.3f, 3f, 0);
         gl.glRotatef(60,-1, 0, 0);
         gl.glBegin(gl.GL_POINTS);
            for(int i=0; i<200; i++){
               float x = (float) Math.cos(i*5*3.14/200);
               float y = (float) Math.sin(i*5*3.14/200);
               gl.glVertex3f(x, y+5,0); gl.glColor3f(1.0f, 1f, 1f);
            }
        gl.glEnd();
        gl.glPopMatrix();
        // tierra
        gl.glPushMatrix();
          
        gl.glTranslatef((float )translate+2.2f, (float) traslatey-4.5f, -15.0f);
        gl.glScalef(0.35f, 0.35f, 0f);
        
        gl.glRotatef(ang,0, 1, 0.5f);
        gl.glTranslatef(3.0f, 5.0f, -4f);
        gl.glColor3f(0f, 0f, 1.5f);    // Set the current drawing color to light blue
        glut.glutSolidSphere(1, 20, 20);// Bottom Left
       
        gl.glPopMatrix();
        
        //orbita marte
        gl.glPushMatrix();
         gl.glTranslatef(1f, -10.2f , -20.0f);
        gl.glScalef(4.3f, 4f, 0);
         gl.glRotatef(60,-1, 0, 0);
         gl.glBegin(gl.GL_POINTS);
            for(int i=0; i<200; i++){
               float x = (float) Math.cos(i*5*3.14/200);
               float y = (float) Math.sin(i*5*3.14/200);
               gl.glVertex3f(x, y+5,0); gl.glColor3f(1.0f, 1f, 1f);
            }
        gl.glEnd();
        gl.glPopMatrix();
       //marte
       gl.glPushMatrix();
          
        gl.glTranslatef((float )translate+2.2f, (float) traslatey-5.5f, -15.0f);
        gl.glScalef(0.35f, 0.35f, 0f);
        
        gl.glRotatef(ang,0, 1, -0.5f);
        gl.glTranslatef(9.0f, 5.0f, -4f);
        gl.glColor3f(1.5f, 0f, 0.0f);    // Set the current drawing color to light blue
        glut.glutSolidSphere(1, 20, 20);// Bottom Left
        
        gl.glPopMatrix();
        
        //orbita jupiter
        gl.glPushMatrix();
         gl.glTranslatef(1f, -12.8f , -20.0f);
        gl.glScalef(5.3f, 5f, 0);
         gl.glRotatef(60,-1, 0, 0);
         gl.glBegin(gl.GL_POINTS);
            for(int i=0; i<200; i++){
               float x = (float) Math.cos(i*5*3.14/200);
               float y = (float) Math.sin(i*5*3.14/200);
               gl.glVertex3f(x, y+5,0); gl.glColor3f(1.0f, 1f, 1f);
            }
        gl.glEnd();
        gl.glPopMatrix();
        //jupiter
       gl.glPushMatrix();
          
        gl.glTranslatef((float )translate+2.4f, (float) traslatey-5f, -15.0f);
        gl.glScalef(0.5f, 0.5f, 0f);
        
        gl.glRotatef(ang,0, 1, 0.5f);
        gl.glTranslatef(2f, 6.5f, -5.9f);
        gl.glColor3f(2.5f, 1f, 1.0f);    // Set the current drawing color to light blue
        glut.glutSolidSphere(1, 20, 20);// Bottom Left
        
        gl.glPopMatrix();
        
        //orbita de saturno 
        gl.glPushMatrix();
         gl.glTranslatef(1f, -15.4f , -20.0f);
        gl.glScalef(6.3f, 6f, 0);
         gl.glRotatef(60,-1, 0, 0);
         gl.glBegin(gl.GL_POINTS);
            for(int i=0; i<200; i++){
               float x = (float) Math.cos(i*5*3.14/200);
               float y = (float) Math.sin(i*5*3.14/200);
               gl.glVertex3f(x, y+5,0); gl.glColor3f(1.0f, 1f, 1f);
            }
        gl.glEnd();
        gl.glPopMatrix();
        //saturno
       gl.glPushMatrix();
          
        gl.glTranslatef((float )translate+2.2f, (float) traslatey-5.6f, -15.0f);
        gl.glScalef(0.55f, 0.55f, 0f);
        
        gl.glRotatef(ang,0, 1, -0.5f);
        gl.glTranslatef(8f, 2.7f, -4.5f);
        gl.glColor3f(0f, 1f, 0.0f);    // Set the current drawing color to light blue
        glut.glutSolidSphere(1, 20, 20);// Bottom Left
         glut.glutWireTorus(1.5f, 1.5f, 30, 2);
        gl.glPopMatrix();
         //orbita de urano
        gl.glPushMatrix();
         gl.glTranslatef(1f, -18f , -20.0f);
        gl.glScalef(7.3f, 7f, 0);
         gl.glRotatef(60,-1, 0, 0);
         gl.glBegin(gl.GL_POINTS);
            for(int i=0; i<200; i++){
               float x = (float) Math.cos(i*5*3.14/200);
               float y = (float) Math.sin(i*5*3.14/200);
               gl.glVertex3f(x, y+5,0); gl.glColor3f(1.0f, 1f, 1f);
            }
        gl.glEnd();
        gl.glPopMatrix();
         //urano
       gl.glPushMatrix();
          
        gl.glTranslatef((float )translate+2.3f, (float) traslatey-5.5f, -15.0f);
        gl.glScalef(0.55f, 0.55f, 0f);
        
        gl.glRotatef(ang,0, 1, 0.5f);
        gl.glTranslatef(8.5f, 6.5f, -4f);
        gl.glColor3f(0f, 1f, 1.0f);    // Set the current drawing color to light blue
        glut.glutSolidSphere(1, 20, 20);// Bottom Left
        
        gl.glPopMatrix();
          //orbita de neptuno
        gl.glPushMatrix();
         gl.glTranslatef(1f, -20.6f , -20.0f);
        gl.glScalef(8.3f, 8f, 0);
         gl.glRotatef(60,-1, 0, 0);
         gl.glBegin(gl.GL_POINTS);
            for(int i=0; i<200; i++){
               float x = (float) Math.cos(i*5*3.14/200);
               float y = (float) Math.sin(i*5*3.14/200);
               gl.glVertex3f(x, y+5,0); gl.glColor3f(1.0f, 1f, 1f);
            }
        gl.glEnd();
        gl.glPopMatrix();
         //neptuno
       gl.glPushMatrix();
          
        gl.glTranslatef((float )translate+2.3f, (float) traslatey-8.55f, -15.0f);
        gl.glScalef(0.75f, 0.75f, 0f);
        
        gl.glRotatef(ang,0, 1, -0.5f);
        gl.glTranslatef(9.1f, 6.5f, -4f);
        gl.glColor3f(0.9f, 0.9f, 0.9f);    // Set the current drawing color to light blue
        glut.glutSolidSphere(1, 20, 20);// Bottom Left
        
        gl.glPopMatrix();
        
        //sol
        gl.glPushMatrix();
         gl.glTranslatef(1f, 0.5f , -20.0f);
        gl.glScalef(0.5f, 0.5f, 0f);
        gl.glRotatef(ang,0, 1, 0.5f);
        
        gl.glColor3f(1f, 1f, 0f);    // Set the current drawing color to light blue
        glut.glutSolidSphere(2, 20, 20);// Bottom Left
        gl.glPopMatrix();
        
        //
        gl.glPushMatrix();
         gl.glTranslatef(tra-5f, tra-4 , -20.0f);
         gl.glScalef(0.1f, 0.1f, 0.1f);
         gl.glRotatef(ang,0, 1, 0.5f);
         if (tra<=20) {
             tra+=0.01f;
              gl.glColor3f(1f, 1f, 1f);    // Set the current drawing color to light blue
        glut.glutSolidSphere(2, 20, 20);// Bottom Left 
         }
//             if (tra>=5) {
//                 if (tra1<=7) {
//                     tra1+=0.1f;
//                     gl.glColor3f(1f, 1f, 1f);    // Set the current drawing color to light blue
//                        glut.glutSolidSphere(2, 20, 20);// Bottom Left 
//                 }
//             
//        
//        }
//asteroide
         gl.glPopMatrix();   
         gl.glPushMatrix();
         gl.glTranslatef(tra-5f, tra-4 , -20.0f);
         gl.glScalef(0.5f, 0.5f, 0.5f);
         gl.glRotatef(ang,0, 1, 0.5f);
            if (tra<=20) {
                tra+=0.01f;
             
              gl.glColor3f(0f, 1f, 1f);    // Set the current drawing color to light blue
                glut.glutWireOctahedron();// Bottom Left 
         }        
        
       
        gl.glPopMatrix(); 
        traslatex=traslatex-0.001f;
        rotatey=rotatey+0.0001f;
        rotatez=rotatez+0.001f;
         
        ang++;
        // Flush all drawing operations to the graphics card
        gl.glFlush();
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
}

