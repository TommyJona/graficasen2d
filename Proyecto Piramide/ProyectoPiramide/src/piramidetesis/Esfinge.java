/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramidetesis;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author alexo
 */
public class Esfinge {
    Esfera cabeza,cabeza2,cabeza3,cabeza4,cabeza5,cabeza6;
    Cubo1 cubcabeza,cubcabez2,cubcabez3,cubcabeza4,cubcabez5;
    Cilindro cilindro,cilindro2,cilindro3;
    float x,y,z,w,h,d;
    
    
    public Esfinge( float x, float y, float z, float w, float h, float d) {
       
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.d = d;
        //cabeza
        
       //nariz
       cubcabeza=new Cubo1(0, 0, 0, w/3, h/2 ,d/3, 1, 1, 1);
       
       cabeza=new Esfera(0, 0, 0+cubcabeza.d*2.5f, 1, 1, 0, 1f, 12, 16);
       cabeza3=new Esfera(0.13f, -0.2f, 1.93f, 1, 1, 1, 2.1f, 5, 16/5);
       //cejas
       cubcabez2=new Cubo1(0.3f, 0+cubcabeza.h/3, 0-cubcabeza.d*0.3f, w/3, h/20, d/8, 1, 1, 1);
       cubcabez3=new Cubo1(-0.6f,0, 0, w/3, h/20, d/8, 1, 1, 1);
       //cuerpo cilindrico
       cubcabeza4=new Cubo1(0, 0, 0, w*5, h*3, d*10, 1, 1, 0);
       
       cilindro=new Cilindro(0, 0, 0, 1, 1, 0, 0.5f, 0.5f, 0+h, 20, 20);
       cilindro2=new Cilindro(0, 0, 0, 1, 1, 0, 1.8f, 1.8f, 9+h, 10, 10);
       cilindro3=new Cilindro(0, 0, 0, 1, 1, 0, 1.5f, 2.6f, 2+h, 10, 10);
//       ojos
        cabeza4=new Esfera(0, 0, 0, 1, 0, 0, 0.2f, 20, 20);
        //manos
        cubcabez5=new Cubo1(0, 0, 0, w, h, d+3, 1, 1, 0);
        //parte tracera 
        cabeza6=new Esfera(0, 0, 0, 1, 1, 0, 1.5f, 20, 20);
    }
    public void dibujarEsfinge(){
        
        glPushMatrix();
        //cabeza
        glTranslatef(x, y, z);
        //glRotatef(180, 0, 1, 0);
        cubcabeza.trapecio(w/9);
        cabeza.dibujarEsfera();
        cubcabez2.dibujarCubo();
        cubcabez3.dibujarCubo();
        glPopMatrix();
        glPushMatrix();
        //glRotatef(180, 0, 1, 0);
        glRotatef(-36f, 0.09f, 0, 2f);
        cabeza3.dibujarEsfera();
        glPopMatrix();
        //cuerpo
        glPushMatrix();
        glTranslatef(0, -3, 5.8f);
        cubcabeza4.trapecio(w*1f);
        glPopMatrix();
         glPushMatrix();
        glTranslatef(0, -2.5f, 0.8f);
        cilindro2.dibujarcilindro();
        glPopMatrix();
        //pechoesfinge
         glPushMatrix();
        glTranslatef(0, -1.5f, 10.5f);
        glRotatef(90, 1, 0, 0);
        cilindro3.dibujarcilindro();
        glPopMatrix();
        
        //ojos 
        glPushMatrix();
        glTranslatef(0.23f, 0.01f, 0.06f);
        cabeza4.dibujarEsfera();
        glPopMatrix();
        glPushMatrix();
        glTranslatef(-0.23f, 0.01f, 0.06f);
        cabeza4.dibujarEsfera();
        glPopMatrix();
//        dibujar manos y pies
        glPushMatrix();
        glTranslatef(1, -4, -1);
        cubcabez5.dibujarCubo();        
        glPopMatrix();
        glPushMatrix();
        glTranslatef(1, -3.5f, -3);
        glRotatef(90, 1, 0, 0);
        cilindro.dibujarcilindro();
        glPopMatrix();
        glPushMatrix();
        glTranslatef(-1, -4, -1);
        cubcabez5.dibujarCubo();        
        glPopMatrix();
        glPushMatrix();
        glTranslatef(-1, -3.5f, -3);
        glRotatef(90, 1, 0, 0);
        cilindro.dibujarcilindro();
        glPopMatrix();
        //PIE DERECHO
        glPushMatrix();
        glTranslatef(2.8F, -4, 8);
        cubcabez5.dibujarCubo();        
        glPopMatrix();
        glPushMatrix();
        glTranslatef(2.8F, -3.5f, 6);
        glRotatef(90, 1, 0, 0);
        cilindro.dibujarcilindro();
        glPopMatrix();
        glPushMatrix();
        glTranslatef(2.8F, -3.5f, 10);
        glRotatef(90, 1, 0, 0);
        cilindro.dibujarcilindro();
        glPopMatrix();
        //PIE IZQUIERDO
        glPushMatrix();
        glTranslatef(-2.8F, -4, 8);
        cubcabez5.dibujarCubo();        
        glPopMatrix();
        glPushMatrix();
        glTranslatef(-2.8F, -3.5f, 6);
        glRotatef(90, 1, 0, 0);
        cilindro.dibujarcilindro();
        glPopMatrix();
        glPushMatrix();
        glTranslatef(-2.8F, -3.5f, 10);
        glRotatef(90, 1, 0, 0);
        cilindro.dibujarcilindro();
        glPopMatrix();
        //parte tracera
        glPushMatrix();
        glTranslatef(0,-2.2f,10.7f);
        cabeza6.dibujarEsfera();
        glPopMatrix();
        
    }
}
