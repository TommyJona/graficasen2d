/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramidetesis;
import static org.lwjgl.opengl.GL11.*;
/**
 *
 * @author alexo
 */
public class Faraon {
    Esfera cabeza,corona;
    Cubo1 cuerpo,cinturon;
    Cilindro piesm;
    public Faraon(){
        cabeza=new Esfera(0, 0, 0, 1, 1, 0, 1.7f, 20, 20);
        corona=new Esfera(0, 0, 0, 1, 0, 0, 3.5f, 5, 5);
        cuerpo=new Cubo1(0, 0, 0, 6, 5, 2, 0, 1, 0);
        piesm=new Cilindro(0, 0, 0, 1, 1, 0, 0.7f, 0.7f, 6, 20, 20);
        //accesorios
        cinturon=new Cubo1(0, 0, 0, 2.1f, 1, 3.5f, 1, 0, 0);
    }
public void dibujarfaraon(){
    //cabeza
    glPushMatrix();
    glTranslatef(0.4f, 10, 0);
    glRotatef(90, 0, 1, 0);
    cabeza.dibujarEsfera();
    glPopMatrix();
    //corona
    glPushMatrix();
    glTranslatef(-0.3f, 10, 0);
    glRotatef(90, 0, 1, 0);
    glRotatef(180, 0, 0, 1);
    glScalef(1.1f, 1.1f, 0.6f);
    corona.dibujarEsfera();
    glPopMatrix();
    //cuepo
    glPushMatrix();
    glTranslatef(0, 5, 0);
    glRotatef(90, 0, 1, 0);
    glRotatef(180, 0, 0, 1);
    cuerpo.trapecio(1.5f);
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(0, 0f, 0);
    glRotatef(90, 0, 1, 0);
    cuerpo.trapecio(1.5f);
    glPopMatrix();
    //manos
    glPushMatrix();
    glTranslatef(0, 7.5f, 3.6f);
    glRotatef(90, 1, 0, 0);
    piesm.dibujarcilindro();
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(0, 7.5f, -3.6f);
    glRotatef(90, 1, 0, 0);
    piesm.dibujarcilindro();
    glPopMatrix();
    //pies 
    glPushMatrix();
    glTranslatef(0, -1.5f, 1.5f);
    glRotatef(90, 1, 0, 0);
    glScalef(1.3f, 1.3f, 1.3f);
    piesm.dibujarcilindro();
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(0, -1.5f, -1.5f);
    glRotatef(90, 1, 0, 0);
    glScalef(1.3f, 1.3f, 1.3f);
    piesm.dibujarcilindro();
    glPopMatrix();
    //accesorios
    glPushMatrix();
    glTranslatef(0, 3f, 0);
    cinturon.dibujarCubo();
    glPopMatrix();  
    
}
}
