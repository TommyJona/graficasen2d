package piramidetesis;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;
import org.lwjgl.util.glu.Sphere;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import org.lwjgl.util.glu.Cylinder;

public class Main {
    
    public static Camara cam,cam1;
    public static float ang=0,x=0,y=0,ang1=0,encendido=0,apagado=0;
     public static float trax=0,traz=0,rotx=0,roty=0,trahx=0,trahz=0;
    public static Cilindro cilin=new Cilindro(0, 0, 0, 0, 1, 0, 1, 1, 4, 10, 10);
    
   
    public static void main(String[] args) {
        initDisplay();
        initGL();
        gameLoop();
        cleanUp();
    }

    public static void initGL() {
       cam=new Camara(90 , (float)Display.getWidth()/(float) Display.getHeight(),0.1f, 1800);
//       cam1=new Camara(70 , (float)Display.getWidth()/(float) Display.getHeight(),0.3f, 500);
        glClearColor(0, 1, 1, 1);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_COLOR_MATERIAL);
        glEnable(GL_CULL_FACE_MODE);
        glEnable(GL_BACK);
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND); 
       
    }

    public static void gameLoop() {

       Cilindro ci=new Cilindro(0, 0, 0, 1, 1, 1, 3, 3, 2, 20, 20);
       AntiguoEgipt ant=new AntiguoEgipt();
       
       Esfera esf=new Esfera(0, 1, 0, 1, 1, 0, 5, 20, 20);
       cubo cub=new cubo();
       cubo cub2=new cubo();
       Cubo1 cub1=new Cubo1(0, 0, 0, 1, 1,1, 1, 0, 0);
       Cubo1 cub6=new Cubo1(0, 0, 0, 1, 1,1, 1, 1, 0);
       Piramide1 pi1=new Piramide1(0, 0, 0, 11, 11, 15, 1, 1, 1);
       Esfinge esfinge=new Esfinge(0, 0, 0, 1, 1, 1);
       CasayPilares casapilares=new CasayPilares(0, 0, 0, 1, 1, 1, 1, 1, 1);
       Piramide pi=new Piramide();
       Escenarios esc=new Escenarios();
       Templo templo=new Templo();
       Faraon fa=new Faraon();
       
       String [][]matriz=esc.escenarioCuatro();
       String [][]matriz2=esc.escenarioCinco();
       String [][]matriz3=esc.escenarioCinco();
       String [][]matriz4=esc.escenarioCinco();
       Texture text=loadTexture("pared");
       Texture text2=loadTexture("cesped");
       Texture text3=loadTexture("tierra");

        while (!Display.isCloseRequested()) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glShadeModel(GL_SMOOTH);
            glLoadIdentity();

                 cam.userview();
            //camara elipse
            if ((float)encendido==2) {
                cam.moverot(ang1);
                ant.divujarAntiguoEgipt();
            }
            //camara que gira en un punto 
            if ((float)encendido==4) {
                cam.movercentro(ang1);
                ant.divujarAntiguoEgipt();
            }
//            camra automatica
//            glTranslatef(1170, 25, 600);
            
                
//                glPushMatrix();
                 if (encendido==5) {
//                glTranslatef(1170, 25, 600);
//                   moverusuario();
//                  glRotatef(180, 0, 1, 0);
//                  fa.dibujarfaraon();
//                if(traz<4) {
//                  traz+=0.1f;
//                  cam.moveZ(traz);
//                  moverhombrez(-traz);
////                   moverusuario();
//                  
//                  glPushMatrix();
//                  glTranslatef(1150, 25, 600);
//                  moverusuario();
//                  glRotatef(180, 0, 1, 0);
//                  fa.dibujarfaraon();
//                  glPopMatrix();
//                    }
//                     if (traz>=4) {
//                         if (traz<=5) {
//                           traz+=0.1f;
//                  cam.moveX(traz);
//                  moverhombrex(-traz);
////                   moverusuario();
//                  
//                  glPushMatrix();
//                  glTranslatef(1150, 25, 600);
//                  moverusuario();
//                  glRotatef(180, 0, 1, 0);
//                  fa.dibujarfaraon();
//                  glPopMatrix();  
//                         }
//                     }
            glPushMatrix();
//             glRotatef(180, 0, 1, 0);
             glTranslatef(1140-trahz, 25, 600+trahx);

             if (trahz<15) {
                trahz=(trahz+0.5f);
               cam.moveZ(trahz/8);
             fa.dibujarfaraon();
             }
             if (trahz>=15) {
                fa.dibujarfaraon();
                
                if (trahx>=-15) {
                      trahx-=0.5f;
                   cam.moveX(trahx/12);
                }
                       
                     }
                 }
             glPopMatrix();
//           
                glPushMatrix();
                 
                 ant.divujarAntiguoEgipt();
                glPopMatrix();
                        
           //        dibujarfaraon
//        glTranslatef(1140, 15, 600);
//        glTranslatef(-traz, -y, -trax);
//        glRotatef(ang1, 0.0f, 1.0f, 0.0f);
       
        
        glPushMatrix();
        moverusuario();
//        glTranslatef(traz, y, trax);
////        moverhombre(ang1);
        glRotatef(180, 0, 1, 0);
//        glRotatef(ang1, 0, 1, 0);
        
        fa.dibujarfaraon();
        glPopMatrix();
        
            
            
           
            ang1=x;
            Display.update();
           encendido=apagado;
        controles();
        
        }
       
    }

    public static void controles() {
        boolean forward=Keyboard.isKeyDown(Keyboard.KEY_W);
        boolean backward=Keyboard.isKeyDown(Keyboard.KEY_S);
        boolean left=Keyboard.isKeyDown(Keyboard.KEY_A);
        boolean right=Keyboard.isKeyDown(Keyboard.KEY_D);
        boolean arriba=Keyboard.isKeyDown(Keyboard.KEY_Q);
        boolean abajo=Keyboard.isKeyDown(Keyboard.KEY_R);
        boolean flechaa=Keyboard.isKeyDown(Keyboard.KEY_UP);
        boolean flechaab=Keyboard.isKeyDown(Keyboard.KEY_DOWN);
        boolean flechader=Keyboard.isKeyDown(Keyboard.KEY_RIGHT);
        boolean flechaizq=Keyboard.isKeyDown(Keyboard.KEY_LEFT);
        boolean pricamara=Keyboard.isKeyDown(Keyboard.KEY_1);
        boolean segcamara=Keyboard.isKeyDown(Keyboard.KEY_2);
        boolean tercamara=Keyboard.isKeyDown(Keyboard.KEY_3);
        boolean cuarcamara=Keyboard.isKeyDown(Keyboard.KEY_4);
        boolean quincamara=Keyboard.isKeyDown(Keyboard.KEY_5);
        boolean camarax=Keyboard.isKeyDown(Keyboard.KEY_X);
        boolean camaraz=Keyboard.isKeyDown(Keyboard.KEY_Z);
        
        float mx= Mouse.getDX();
        float my=Mouse.getDY();
         
         mx*=0.1f;
         my*=0.1f;
         
        
        if (Mouse.isButtonDown(0)) {
          cam.rotRY(mx);
          cam.rotRX(-my);
//        x=ang+0.01f;
        }
        if (arriba) {
            cam.moveY(-2f);
            
        }
        if (abajo) {
            cam.moveY(2f);
            
        }
        if (forward) {
            cam.moveZ(2f);
            
            
        }
        if (backward) {
            cam.moveZ(-2f);
//             traz-=2f;
        }
        if (left) {
            cam.moveX(2f);
//            trax+=2f;
        }
        if (right) {
            cam.moveX(-2f);
//            trax-=2f;
        }
        if (pricamara){
          
//            x=ang1+0.1f;
           cam.loccamara(4, -5, 10);
             
        }
        if(tercamara){
            cam.loccamara(-1170, -25, -600);
            trax=1140;
            traz=600;
            y=15;
            x=ang+0; 
            
//            apagado=1;
        }
        if(cuarcamara){
            cam.loccamara(-1170, -35, -600);
            apagado=4;
            x=ang+3f;
        }
        if(quincamara){
             cam.loccamara(-1170, -25, -600);
             apagado=5;
        }
        if(segcamara){
           
           cam.loccamara(-1270, -55, -600);
           apagado=2;
            x=ang+3f;
            
//          cam.moverotx(0);
//           x=0;
//            trax=0;
//            traz=0;
//            x=0;
//            mx=0.1f;
//            my=0.1f;
//            cam.rotRY(mx);
//            cam.rotRY(-my);
           
        }
        if (flechaa) {
//            
//            cam.moveZ(2f);
//             for(int i=0; i<100; i++){
//            
//               float x = ((float) Math.cos((i)*2*3.14/100))*1;
//               float y= (float) Math.sin((i)*2*3.14/100)*1;
//                 traz-=x;
//                 trax-=y;
//             }
            
//              traz+=2f+x; 
//            cam.moveZ(2f);
//            cam.moverotx(+2f);
//            x=ang+1f; 
//                cam.moveZ(2f);
//             traz-=2f;
             
            
            cam.moveZ(2f);
            moverhombrez(-2);
        }
         if (flechaab) {
//            traz+=2f;
//            cam.moveZ(-2f);
//            cam.moverotx(+2f);
//            x=ang+1f; 
//            moverhombre(2f);
           cam.moveZ(-2f);
             moverhombrez(2);
              
        }
          if (flechader) {
               //for(int i=0; i<100; i++){
//            
//               float rotx = ((float) Math.cos((i)*2*3.14/100))*13/3;
//               float roty= (float) Math.sin((i)*2*3.14/100)*13/3;
//                 glTranslatef(rotx, roty, 0);
//              roty-=0.01f;
//              trax-=0.008f;
//            
//              cam.rotRY(2f);
//               cam.moveZ(2f);
//            cam.moverotx(+2f);
//            x=ang+1f; 
//            moverx(-2f);
            cam.moveX(-2f);
//            trax-=2f;
              moverhombrex(-2f);
        }
          if (flechaizq) {
//            trax+=2f;
            cam.moveX(+2f);
              moverhombrex(2f);
        }
          if (camarax) {
//              cam.moverotx(-2f);
            cam.moverotx(2f);
              rotarhombre(-2f);
              
//              rotarhombre(2f);
//            x=ang1-2.1f;
//            moverhombre(1.2f);
//            cam.rotRY(-0.1f);
//            cam.moveZ(2f);
//            cam.moverotx(-0.015f);
//            cam.moveZ(-2f);
//            cam.moveZ(2f);
//            cam.moveX(+2f);
//            cam.moveX(-2f);
        }
          if (camaraz) {
            cam.moverotx(-2f);
            rotarhombre(2f);
            
//            rotarhombre(-2f);
//            x=ang1+2.1f;
//            moverhombre(-1.2f);
//            cam.rotRY(0.1f);
//            cam.rotRY(2f);
//            cam.moveZ(2f);
            
            
//            cam.moverotx(0.015f);
//            cam.moveZ(2f);
//            cam.moveZ(-2f);
//            cam.moveX(+2f);
//            cam.moveX(-2f);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
            cleanUp();
        }
    }
     public static void moverhombrex(float amt){
        traz+=amt*Math.sin(Math.toRadians(roty+90));
        trax+=amt*Math.cos(Math.toRadians(roty+90));
        
         
    }
     public static void moverhombrez(float amt){
        traz+=amt*Math.sin(Math.toRadians(roty));
        trax+=amt*Math.cos(Math.toRadians(roty));
         
        
    }
      public static void rotarhombre(float amt){
        traz-=amt*Math.sin(Math.toRadians(roty));
        trax-=amt*Math.cos(Math.toRadians(roty));
        roty-=amt;
        
    }
    
     public static void rotary(float amt){
         roty-=amt;
     }
     public static void moverusuario(){
         glTranslatef(trax, y, traz);
         glRotatef(-roty, 0, 1, 0);
         
     }
      public static void moverusuario1(){
         glTranslatef(trax, y, traz);
         glRotatef(0, 0, 1, 0);
         
     }
//      public static void moverx(float amt){
//        traz+=amt*Math.sin(Math.toRadians(roty));
//        trax+=amt*Math.cos(Math.toRadians(roty));
//        roty+=amt;
//    }
//      public static void RY(float amt){
//        roty+=0;
//        
//    }

    public static void cleanUp() {
        Display.destroy();
        System.exit(0);
    }

    public static void initDisplay() {
        try {
            Display.setDisplayMode(new DisplayMode(800, 600));
            Display.setTitle("Lienzo. Lwjgl: " + Sys.getVersion());
            Display.create();
        } catch (LWJGLException ex) {
            ex.printStackTrace();
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
    }

    public static Texture loadTexture(String key) {
        try {
            return TextureLoader.getTexture("png", new FileInputStream(
                    new File("res/" + key + ".png")));

        } catch (IOException ex) {
            Logger.getLogger(Main.class
                    .getName()).log(
                            Level.SEVERE, null, ex);
        }
        return null;
    }

    public static FloatBuffer asFloatBuffer(float[] arreglo) {
        FloatBuffer fb = BufferUtils.createFloatBuffer(arreglo.length);
        fb.put(arreglo);
        fb.flip();
        return fb;
    }

    public static IntBuffer asIntBuffer(int[] arreglo) {
        IntBuffer ib = BufferUtils.createIntBuffer(arreglo.length);
        ib.put(arreglo);
        ib.flip();
        return ib;
    }
}
