/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramidetesis;

import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author alexo
 */
public class CasayPilares {
     Escenarios esc=new Escenarios();
       
    String [][]matriz=esc.escenarioUno();
    String [][]matriz1=esc.escenarioUno();
    String [][]matriz2=esc.escenarioUnomod();
    Cubo1 base,base2,casa,entrada;
    
    Cilindro cilindro,cilindro2;
    Piramide1 piramide,piramide2;
    Esfinge esfinge;
    float x,y,z,c1,c2,c3,w,h,d;
    Palmeras palmera;
    public CasayPilares(float x,float y,float z,float c1,float c2,float c3,float w,float h,float d){
        this.x=x;
        this.y=y;
        this.z=z;
        this.c1=c1;
        this.c2=c2;
        this.c3=c3;
        this.w=w;
        this.h=h;
        this.d=d;
        base=new Cubo1(0, 0, 0, w*2, h*10, d*2, c1, c2-0.5f, c3);
        piramide=new Piramide1(0, base.h-4.5f, 0, w-0.33f, h, d*2.0f, c1-0.5f, c2-0.5f, c3);
        base2=new Cubo1(0, 0, 0, w*2, h*10, d*2, c1, c2-0.5f, c3);
        piramide2=new Piramide1(0, base.h-4.5f, 0, w-0.33f, h, d*2.0f, c1-0.5f, c2-0.5f, c3);
        casa=new Cubo1(0, 0, 0, 1, 1, 1, 0, 1, 0);
        cilindro=new Cilindro(0, 0, 0, 0, 0, 1, 0.75f, 0.75f, 4, 10, 10);
        cilindro2=new Cilindro(0, 0, 0, 0, 0, 1, 0.75f, 0.75f, 6, 10, 10);
        
        //entrada palacio
        entrada=new Cubo1(0, 0, 0, 1.5f, 1f, 2.8f, 1, 1, 1);
        esfinge=new Esfinge(0, 0, 0, 1, 1, 1);
        palmera=new Palmeras(0, 0, 0, 1, 0, 0, 1, 1, 1);
    }
    public void dibujarcasaPilares (){
//        base
        glPushMatrix();
        glTranslatef(4, 0, 0);
        base.trapecio(w/1.5f);
        piramide.dibujarPiramide();
        glPopMatrix();
        glPushMatrix();
        glTranslatef(8, 0, 0);
        base2.trapecio(w/1.5f);
        piramide2.dibujarPiramide();
        glPopMatrix();
        // casa
        glPushMatrix();
        glTranslatef(-2, 3, 11);
        glScalef(1, 2, 1);
        pilares();
        glPopMatrix();
        //techo superior
        glPushMatrix();
         glTranslatef(-2.175F, 4, 11);
        glScalef(1.03f, 2, 1);
        techosuperior();
        glPopMatrix();
        //pilares interiores 
        glPushMatrix();
        glTranslatef(-2, 7, 11);
        glScalef(1, 2, 1);
        pilaresinteriores();
        glPopMatrix();
        //techo superior superior
        glPushMatrix();
        glTranslatef(-2.49F, 8, 11);
        glScalef(1.08f, 2, 1);
        techosuperiorsup();
        glPopMatrix();
        //entrada de la casa
        glPushMatrix();
        glTranslatef(4f, -4.5f, 8);
        entrada.trapecio(2/10f);
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(8f, -4.5f, 8);
        entrada.trapecio(2/10f);
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(4f, -3.1f, 7.3f);
        glScalef(0.2f, 0.2f, 0.15f);
        esfinge.dibujarEsfinge();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(8f, -3.1f, 7.3f);
        glScalef(0.2f, 0.2f, 0.15f);
        esfinge.dibujarEsfinge();
        glPopMatrix();
        //palmeras exteriores
        glPushMatrix();
        palmerascasa();
        glPopMatrix();
        //transparencia
//        glPushMatrix();
//      
//        cu.dibujarCubo();
//        glPopMatrix();
//        
//      
    }
    public void dibujaresfinge(){
        //entrada de la casa
        glPushMatrix();
        glTranslatef(4f, -4.5f, 8);
        entrada.trapecio(2/10f);
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(4f, -3.1f, 7.3f);
        glScalef(0.2f, 0.2f, 0.15f);
        esfinge.dibujarEsfinge();
        glPopMatrix();
    }
    public void pilares(){
        glRotatef(90, 1, 0, 0);
         for (int x = 0; x <matriz.length; x++) {
                for (int z = 0; z < matriz[x].length; z++) {
                   glTranslated(x,z,0);
                    if (matriz[x][z].equals("1")) {
                        
                       cilindro.dibujarcilindro();
                    }
                    glTranslated(-x,-z,0);
               }
           }
    }
    public void techosuperior(){
        for (int x = 0; x <matriz2.length; x++) {
                for (int z = 0; z < matriz2[x].length; z++) {
                   glTranslated(x,0,z);
                    if (matriz2[x][z].equals("2")) {
                        
                       casa.dibujarCubo();
                    }
                    glTranslated(-x,0,-z);
               }
           }
    }
    public void pilaresinteriores(){
        glRotatef(90, 1, 0, 0);
        for (int x = 0; x <matriz.length; x++) {
                for (int z = 0; z < matriz[x].length; z++) {
                   glTranslated(x,z,0);
                    if (matriz[x][z].equals("5")) {
                        
                       cilindro2.dibujarcilindro();
                    }
                    glTranslated(-x,-z,0);
               }
           }
    }
    public void techosuperiorsup(){
         for (int x = 0; x <matriz2.length; x++) {
                for (int z = 0; z < matriz2[x].length; z++) {
                   glTranslated(x,0,z);
                    if (matriz2[x][z].equals("4")) {
                        
                       casa.dibujarCubo();
                    }
                    glTranslated(-x,0,-z);
               }
           }
    }
    public void palmerascasa(){
        for (int x = 0; x <matriz1.length; x++) {
                for (int z = 0; z < matriz1[x].length; z++) {
                   glTranslated(x,0,z);
                    if (matriz1[x][z].equals("7")) {
                        palmera.dibujarPalmeras();
                       
                    }
                    glTranslated(-x,0,-z);
               }
           }
    }
    
}
