/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramidetesis;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author alexo
 */
public class Piramide {
    public void drawPiramide(float size ){
         glBegin(GL_TRIANGLES);
 
        // Front
       glTexCoord2d(0, 0);glVertex3f(0.0f, 4.0f, 0.0f);
       glTexCoord2d(2, 2);glVertex3f(-4.0f, -4.0f, 4.0f);
      glTexCoord2d(2, 0); glVertex3f(4.0f, -4.0f, 4.0f);
 
        // Right Side Facing Front
       glTexCoord2d(0, 0);glVertex3f(0.0f, 4.0f, 0.0f);
       glTexCoord2d(2, 2);glVertex3f(4.0f, -4.0f, 4.0f);
       glTexCoord2d(2, 0);glVertex3f(0.0f, -4.0f, -4.0f);
 
        // Left Side Facing Front
       glTexCoord2d(0, 0);glVertex3f(0.0f, 4.0f, 0.0f);
       glTexCoord2d(2, 2);glVertex3f(0.0f, -4.0f, -4.0f);
       glTexCoord2d(2, 0);glVertex3f(-4.0f, -4.0f, 4.0f);
 
        // Bottom
       glTexCoord2d(0, 0);glVertex3f(-4.0f, -4.0f, 4.0f);
       glTexCoord2d(2, 2);glVertex3f(4.0f, -4.0f, 4.0f);
       glTexCoord2d(2, 0);glVertex3f(0.0f, -4.0f, -4.0f);
 
       glEnd();
    }
    public void drawPiramide2( float size){
         glBegin(GL_TRIANGLES);
 
        // Front
        
        glTexCoord2d(0, 0); glVertex3f(0.0f, 2.0f, 0.0f);
     glTexCoord2d(2, 2);glVertex3f(-2.0f, -2.0f, 2.0f);
      glTexCoord2d(2, 0);glVertex3f(2.0f, -2.0f, 2.0f);
 
        // Right Side Facing Front
      glTexCoord2d(0, 0);glVertex3f(0.0f, 2.0f, 0.0f);
      glTexCoord2d(2, 2);glVertex3f(2.0f, -2.0f, 2.0f);
      glTexCoord2d(2, 0);glVertex3f(0.0f, -2.0f, -2.0f);
 
        // Left Side Facing Front
      glTexCoord2d(0, 0);glVertex3f(0.0f, 2.0f, 0.0f);
      glTexCoord2d(2, 2);glVertex3f(0.0f, -2.0f, -2.0f);
      glTexCoord2d(2, 0);glVertex3f(-2.0f, -2.0f, 2.0f);
 
        // Bottom
      glTexCoord2d(0, 0);glVertex3f(-2.0f, -2.0f, 2.0f);
      glTexCoord2d(2, 2);glVertex3f(2.0f, -2.0f, 2.0f);
      glTexCoord2d(2, 0);glVertex3f(0.0f, -2.0f, -2.0f);
 
       glEnd();
    }
}
