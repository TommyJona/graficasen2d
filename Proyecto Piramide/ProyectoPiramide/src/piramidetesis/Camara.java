
package piramidetesis;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;


public class Camara {
    private float x,y,z,rx,ry,rz;
    
    private float fov,aspect,near,far;
     public Camara(float fov,float aspect, float near, float far){
         x=-1150;
         y=-15;
         z=-300;
//         this.x=x;
//         this.y=y;
//         this.z=z;
//         rx=0;
//         ry=180;
//         rz=0;
//        this.rx=x;
//         this.rz=z;
         ry=-90;
        
         this.aspect=aspect;
         this.fov=fov;
         this.far=far;
         this.near=near;
         initprojection();
     } 
private void initprojection(){//matrices para el modelado de los objetos en 3d
    glMatrixMode(GL_PROJECTION);//ricebe todas las librerias de opengl
    glLoadIdentity();//reset matrix proyeccion
    gluPerspective(fov, aspect, near, far);//activa la perspectiva
    glMatrixMode(GL_MODELVIEW);//MATRIX DE MODELADO
    glLoadIdentity();//la volvemos a resetear la matrix
     
}   
public void userview(){//metodo para moverse en el espacio 3d
    glRotated(rx, 1, 0, 0);
    glRotated(ry, 0, 1, 0);
    glRotated(rz, 0, 0, 1);
    glTranslatef(x, y, z);
}
public void userview1(){//metodo para moverse en el espacio 3d
    
    glRotated(ry, 0, 1, 0);
    
    
}

   
    public float getX() {
        return x;
    }

    
    public void setX(float x) {
        this.x = x;
    }

    
    public float getY() {
        return y;
    }


    public void setY(float y) {
        this.y = y;
    }

   
    public float getZ() {
        return z;
    }

  
    public void setZ(float z) {
        this.z = z;
    }

   
    public float getRx() {
        return rx;
    }

    
    public void setRx(float rx) {
        this.rx = rx;
    }

   
    public float getRy() {
        return ry;
    }

   
    public void setRy(float ry) {
        this.ry = ry;
    }

    
    public float getRz() {
        return rz;
    }

    
    public void setRz(float rz) {
        this.rz = rz;
    }
    public void moveX(float amt){//ROTACION METODOS ADICIONALES 
        z+=amt*Math.sin(Math.toRadians(ry));
        x+=amt*Math.cos(Math.toRadians(ry));
        
    }
    public void moveY(float amt){
        y+=amt;
    }
    public void moveZ(float amt){
        z+=amt*Math.sin(Math.toRadians(ry+90));
        x+=amt*Math.cos(Math.toRadians(ry+90));
        
    }
    public void rotRX(float amt){
        rx+=amt;
        
    }
    public void rotRY(float amt){
        ry+=amt;
        
    }
    public void rotRZ(float amt){
        rz+=amt;
        
    }
    public void loccamara(float ejex,float ejey, float ejez){
       
        x=ejex;
        y=ejey;
        z=ejez;
        
    }
    //camara elipse
    public void moverotx(float amt){
        z+=amt*Math.sin(Math.toRadians(ry-70));
        x+=amt*Math.cos(Math.toRadians(ry-70));
        ry+=amt;
    }
    
    public void moverot(float amt){
        z+=amt*Math.sin(Math.toRadians(ry))*15;
        x+=amt*Math.cos(Math.toRadians(ry))*15;
        ry+=amt;
    }
    public void movercentro(float amt){
        z+=amt*Math.sin(Math.toRadians(ry))*8;
        x+=amt*Math.cos(Math.toRadians(ry))*8;
        ry+=amt*2;
    }
}
