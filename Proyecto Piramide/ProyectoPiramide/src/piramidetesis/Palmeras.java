/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramidetesis;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author alexo
 */
public class Palmeras {
    Cubo1 cubase;
    Escenarios esc=new Escenarios();
    String [][]matriz1=esc.escenarioUno();
    Cilindro cilindro3;
    Piramide1 piramide;
    Esfera esfera1;
    Cubo1 cubo;
    float x,y,z,c1,c2,c3,w,h,d;
    public Palmeras(float x,float y,float z,float c1,float c2,float c3,float w,float h,float d){
        this.x=x;
        this.y=y;
        this.z=z;
        this.c1=c1;
        this.c2=c2;
        this.c3=c3;
        this.w=w;
        this.h=h;
        this.d=d;
        cubase=new Cubo1(0, 0, 0, 0.55f, 0.6f, 0.55f, 1, 1, 0);
        cilindro3=new Cilindro(0, 0, 0, 1, 1, 0f, 0.32f, 0.32f, 4, 10, 10);
        esfera1=new Esfera(0, 0, 0, 0.95f, 0.95f, 0, 1, 25, 25);
        piramide=new Piramide1(0, 0, 0, 1, 3, 0.6f, 0, 1, 1);
        cubo=new Cubo1(0, 0, 0, 1, 1, 1, 0, 1, 0);
    }
    public void dibujarPalmeras(){
        //base
        glPushMatrix();
        glTranslatef(-2, -4.7f, 9.1f);
        bases();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(-2, -1, 9.1f);
        glRotatef(90, 1, 0, 0);
        cilindro3.dibujarcilindro();
        glPopMatrix(); 
        //copa palmera 
        glPushMatrix();
        glTranslatef(-2, -0.3f, 9.1f);
        glScalef(0.4f, 0.4f, 0.4f);
        esfera1.dibujarEsfera();
        glPopMatrix();
        //hojas
        glPushMatrix();
        glTranslatef(-1.2f, -0f, 8.1f);
        glScalef(0.6f, 0.5f, 0.8f);
        glRotatef(-120, 1, 1, 0);
        piramide.dibujarPiramide();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(-2f, -0f, 7.6f);
        glScalef(0.6f, 0.5f, 0.8f);
        glRotatef(-90, 1, 0, 0);
        glRotatef(15, 1, 0, 0);
        piramide.dibujarPiramide();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(-3.2f, -0.3f, 9.1f);
        glScalef(0.6f, 0.4f, 0.8f);
        glRotatef(-90, 1, 0, 0);
        glRotatef(15, 1, 0, 0);
        glRotatef(90, 0, 0, 1);
        piramide.dibujarPiramide();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(-1.5f, -0.3f, 10.1f);
        glScalef(0.6f, 0.4f, 0.8f);
        glRotatef(-90, 1, 0, 0);
        glRotatef(15, 1, 0, 0);
        glRotatef(210, 0, 0, 1);
        piramide.dibujarPiramide();
        glPopMatrix();
//        //hojas largas 
//        glPushMatrix();
//        glRotatef(40, 1, 0, 0);
//        glTranslatef(-1.9f, 6.9f, 7.1f);
//        glScalef(1f, 2f, 0.25f);
//        cubo.trapecio(5/20f);
//        glPushMatrix();
//        glTranslatef(0f, 0.5f, 9.7f);
//        glScalef(0.5f, 0.1f, 6.8f);
//        glRotatef(90, 1, 0, 0);
//        piramide.dibujarPiramide();
//        glPopMatrix();
//        glPopMatrix();
        //hojas conjunto
        glPushMatrix();
        glTranslatef(-1.9f, -3.8f, 8.5f);
        glScalef(0.4f,0.4f, 0.3f);
        hojaslargas();
        glPopMatrix();
        //2
        glPushMatrix();
        glTranslatef(-1.7f, -7f, 8f);
        glScalef(0.8f,0.8f, 0.6f);
        hojaslargas();
        glPopMatrix();
    }
    public void dibujarpostes(){
        //base
        glPushMatrix();
        glTranslatef(-2, -4.7f, 9.1f);
        bases();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(-2, -1, 9.1f);
        glRotatef(90, 1, 0, 0);
        cilindro3.dibujarcilindro();
        glPopMatrix(); 
        //copa palmera 
        glPushMatrix();
        glTranslatef(-2, -0.3f, 9.1f);
        glScalef(0.4f, 0.4f, 0.4f);
        esfera1.dibujarEsfera();
        glPopMatrix();
        
    }
    public void bases(){
        for (float i = 0; i < 4; i+=0.2f) {
            glPushMatrix();
            glTranslatef(0,i, 0f);
            cubase.trapecio(2/24f);
            glPopMatrix();
        }
    }
    public void hojaslargas(){
        
        for (int i = 0; i < 20; i++) {
        
          
            float x = ((float) Math.cos((i)*0.095f*33.14/50))/0.9f;
               float y= ((float) Math.sin((i)*0.095f*50.14/50))/0.9f;
              glRotatef(-20, 0, 1, 0);
              
               glTranslatef(x,0, y); 
              
                glPushMatrix();
                glRotatef(-40, 1, 0, 0);
                glTranslatef(-1.9f, 6.9f, 7.1f);
                glScalef(1f, 2f, 0.25f);
                cubo.trapecio(5/20f);
                glPushMatrix();
                glTranslatef(0f, 0.45f, -6.7f);
                glScalef(0.5f, -0.1f, 4.8f);
                glRotatef(-90, 1, 0, 0);
                piramide.dibujarPiramide();
                glPopMatrix();
                glPopMatrix();
        
        
        }
    }
     
}
