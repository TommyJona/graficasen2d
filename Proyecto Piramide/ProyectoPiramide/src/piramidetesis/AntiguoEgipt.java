/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramidetesis;
import static org.lwjgl.opengl.GL11.*;
/**
 *
 * @author alexo
 */
public class AntiguoEgipt {
    Escenarios esc=new Escenarios();
    String [][]matriza=esc.escenarioDos();
    Cubo1 cupiso;
    Piramide1 pi;
    CasayPilares caspi;
    Palmeras palme;
    Templo tem;
    Esfinge esfinge;
    Faraon faraon;
    public AntiguoEgipt(){
       cupiso=new Cubo1(0, 0, 0, 1, 1, 1, 0, 1, 1);
       pi=new Piramide1(0, 0, 0, 11, 11,11, 1, 1, 1);
       caspi=new CasayPilares(0, 0, 0, 1, 1, 1, 1, 1, 1);
       palme=new Palmeras(0, 0, 0, 1, 1, 1, 1, 1, 1);
       tem=new Templo();
       esfinge=new Esfinge(0, 0, 0, 1, 1, 1);
       //faraon
       faraon=new Faraon();
    }
    public void divujarAntiguoEgipt(){
        glPushMatrix();
        glScalef(170, 10, 82);
        dibujarpiso1();
        glPopMatrix();
        //piramides
        glPushMatrix();
        glTranslatef(220, 307f, 610);
        glScalef(50, 55, 50);
        pi.dibujarPiramide();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(112, 143f, 1070);
        glScalef(25, 25, 25);
        pi.dibujarPiramide();
        glPopMatrix();
        
        glPushMatrix();
        glTranslatef(112, 143f, 150);
        glScalef(25, 25, 25);
        pi.dibujarPiramide();
        glPopMatrix();
        //casapilares
        glPushMatrix();
        glTranslatef(800, 67.5f, 100);
        glScalef(12, 12.5f, 12);
        glRotatef(-90, 0, 1, 0);
        caspi.dibujarcasaPilares();
        glPopMatrix();
        //entrada
        glPushMatrix();
        glTranslatef(1140, 130, 400);
        glScalef(25, 25, 25);
        glRotatef(-90, 0, 1, 0);
        dibujarentrada();
        glPopMatrix();
        //templo
        glPushMatrix();
        glTranslatef(900, 53, 820);
        glScalef(4f, 12f, 15.5f);
        tem.dibujartemplo();
        glPopMatrix();
        //dibujar esfinge
        glPushMatrix();
        glTranslatef(1050, 95, 400);
        glScalef(20f, 20f, 20f);
        glRotatef(-90, 0, 1, 0);
        esfinge.dibujarEsfinge();
        glPopMatrix();
//        dibujarfaraon
//        glPushMatrix();
//        glTranslatef(1140, 15, 600);
//        faraon.dibujarfaraon();
//        glPopMatrix();
    }
    public void dibujarpiso1(){
    for (int x = 0; x <matriza.length; x++) {
                for (int z = 0; z < matriza[x].length; z++) {
                    glTranslated(x,0,z);
                    if (matriza[x][z].equals("-")) {
                       cupiso.dibujarCubo();
                    }
                    glTranslated(-x,0,-z);
                }
            }
    }  
    public void dibujarentrada(){
        for (int x = 0; x <matriza.length; x++) {
                for (int z = 0; z < matriza[x].length; z++) {
                    glTranslated(x,0,z);
                    if (matriza[x][z].equals("8")) {
                       palme.dibujarpostes();
                    }
                    glTranslated(-x,0,-z);
                }
            }
    }
//            
//             for (int x = 0; x <matriz4.length; x++) {
//                for (int z = 0; z < matriz4[x].length; z++) {
//                    glTranslated(x,5,z);
//                    if (matriz4[x][z].equals("j")) {
//                        
//                       esfinge.dibujarEsfinge();
//                    }
//                    glTranslated(-x,-5,-z);
//                    
//                }
//                
//            }
//             
//            for (int x = 0; x <matriz.length; x++) {
//                for (int z = 0; z < matriz[x].length; z++) {
//                    glTranslated(x,0,z);
//                    if(matriz[x][z].equals("b")){
//                        cub6.dibujarCubo();
//                    }
//                    glTranslated(-x,0,-z);
//                }
//            }
//            
//            for (int x = 0; x <matriz2.length; x++) {
//                for (int z =0; z < matriz2[x].length; z++) {
//                    glTranslated(x,6,z);
//                    if(matriz2[x][z].equals("w")){
//                        pi1.dibujarPiramide();
//                    }
//                    glTranslated(-x,-6,-z);
//                }
//            }
}
