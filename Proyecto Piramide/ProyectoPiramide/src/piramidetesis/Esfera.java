/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramidetesis;

import com.sun.opengl.util.GLUT;

import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.util.glu.Sphere;


/**
 *
 * @author alexo
 */
public class Esfera {
    
    Sphere esfere;
    float x, y, z;
    float w, h, d;
    float c1, c2, c3,r;
    int slices, stacks;

    public Esfera( float x, float y, float z, float c1, float c2, float c3,float r, int slices, int stacks) {
        
        this.x = x;
        this.y = y;
        this.z = z;
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
        this.r=r;
        this.slices = slices;
        this.stacks = stacks;
    }

     public void dibujarEsfera(){
         glPushMatrix();
         glTranslated(x, y, z);
         glColor3f(c1,c2, c3);
         esfere=new Sphere();
         esfere.draw(r, slices, stacks);
         glPopMatrix();
    }
}
