
package piramidetesis;

import static org.lwjgl.opengl.GL11.*;





public class cubo {
  public void drawcube(float size){
      glBegin(GL_QUADS);
     
      //cara inferior
      glNormal3f(0,0,0);
      
      glTexCoord2d(0, 0); glVertex3f(- size/2, - size/2,  size/2);
      glTexCoord2d(0, 1);glVertex3f( size/2, - size/2,  size/2);
      glTexCoord2d(1, 1);glVertex3f( size/2, - size/2, - size/2);
      glTexCoord2d(1, 0);glVertex3f(- size/2, - size/2, - size/2);
      
      glEnd();
  }  
  public void drawcube2(float size){
      glBegin(GL_QUADS);
     
      //cara inferior
       glNormal3f(1,0,1);
      
      glTexCoord2d(0, 0);glVertex3f(- size/2, - size/2,  size/2);
      glTexCoord2d(0, 1);glVertex3f( size/2, - size/2,  size/2);
      glTexCoord2d(1, 1);glVertex3f( size/2, - size/2, - size/2);
      glTexCoord2d(1, 0);glVertex3f(- size/2, - size/2, - size/2);
      
      glEnd();
  } 
  public void drawcubecomplet(float size){
      glBegin(GL_QUADS);
      //CARA FRONTAL 
      glNormal3f(0,0,1);
      
      glTexCoord2d(0, 0); glVertex3f(- size/2, - size/2,  size/2);//ANTIORARIO DIBUJAR 
      glTexCoord2d(0, 1);glVertex3f( size/2, - size/2,  size/2);
      glTexCoord2d(1, 1);glVertex3f( size/2,  size/2,  size/2);
      glTexCoord2d(1, 0);glVertex3f(- size/2,  size/2,  size/2);
      
      //CARA POSTERIOR
       glNormal3f(0,0,-1);
     
      glTexCoord2d(0, 0);glVertex3f(- size/2, - size/2, - size/2);
      glTexCoord2d(0, 1);glVertex3f( size/2, - size/2, - size/2);
      glTexCoord2d(1, 1);glVertex3f( size/2,  size/2, - size/2);
      glTexCoord2d(1, 0);glVertex3f(- size/2,  size/2, - size/2);
      
      //CARA LATERAL IZQUIERRDA
       glNormal3f(-1,0,0);
      
      glTexCoord2d(0, 0);glVertex3f( size/2, - size/2,  size/2);
      glTexCoord2d(0, 1);glVertex3f( size/2, - size/2, - size/2);
      glTexCoord2d(1, 1);glVertex3f( size/2,  size/2, - size/2);
      glTexCoord2d(1, 0);glVertex3f( size/2,  size/2,  size/2);
      //CARA LATERAL DERACHA
       glNormal3f(1,0,0);
     
      glTexCoord2d(0, 0);glVertex3f(- size/2, - size/2,  size/2);
      glTexCoord2d(0, 1);glVertex3f(- size/2, - size/2, - size/2);
      glTexCoord2d(1, 1);glVertex3f(- size/2,  size/2, - size/2);
      glTexCoord2d(1, 0);glVertex3f(- size/2,  size/2,  size/2);
      //CARA SUPERIOR
       glNormal3f(0,1,0);
      
      glTexCoord2d(0, 0);glVertex3f(- size/2,  size/2,  size/2);
      glTexCoord2d(0, 1);glVertex3f( size/2,  size/2,  size/2);
      glTexCoord2d(1, 1);glVertex3f( size/2,  size/2, - size/2);
      glTexCoord2d(1, 0);glVertex3f(- size/2,  size/2, - size/2);
      //cara inferior
       glNormal3f(0,-1,0);
      
      glTexCoord2d(0, 0);glVertex3f(- size/2, - size/2,  size/2);
      glTexCoord2d(0, 1);glVertex3f( size/2, - size/2,  size/2);
      glTexCoord2d(1, 1);glVertex3f( size/2, - size/2, - size/2);
      glTexCoord2d(1, 0);glVertex3f(- size/2, - size/2, - size/2);
     
      
      glNormal3f(0,0,1);
      
      glTexCoord2d(0, 0); glVertex3f(- size/4, - size/4,  size/4);
      glTexCoord2d(0, 1);glVertex3f( size/4, - size/4,  size/4);
      glTexCoord2d(1, 1);glVertex3f( size/4,  size/4,  size/4);
      glTexCoord2d(1, 0);glVertex3f(- size/4,  size/4,  size/4);
      
      //CARA POSTERIOR
       glNormal3f(0,0,-1);
     
      glTexCoord2d(0, 0);glVertex3f(- size/4, - size/4, - size/4);
      glTexCoord2d(0, 1);glVertex3f( size/4, - size/4, - size/4);
      glTexCoord2d(1, 1);glVertex3f( size/4,  size/4, - size/4);
      glTexCoord2d(1, 0);glVertex3f(- size/4,  size/4, - size/4);
      
      //CARA LATERAL IZQUIERRDA
       glNormal3f(-1,0,0);
      
      glTexCoord2d(0, 0);glVertex3f( size/4, - size/4,  size/4);
      glTexCoord2d(0, 1);glVertex3f( size/4, - size/4, - size/4);
      glTexCoord2d(1, 1);glVertex3f( size/4,  size/4, - size/4);
      glTexCoord2d(1, 0);glVertex3f( size/4,  size/4,  size/4);
      //CARA LATERAL DERACHA
       glNormal3f(1,0,0);
     
      glTexCoord2d(0, 0);glVertex3f(- size/4, - size/4,  size/4);
      glTexCoord2d(0, 1);glVertex3f(- size/4, - size/4, - size/4);
      glTexCoord2d(1, 1);glVertex3f(- size/4,  size/4, - size/4);
      glTexCoord2d(1, 0);glVertex3f(- size/4,  size/4,  size/4);
      //CARA SUPERIOR
       glNormal3f(0,1,0);
      
      glTexCoord2d(0, 0);glVertex3f(- size/4,  size/4,  size/4);
      glTexCoord2d(0, 1);glVertex3f( size/4,  size/4,  size/4);
      glTexCoord2d(1, 1);glVertex3f( size/4,  size/4, - size/4);
      glTexCoord2d(1, 0);glVertex3f(- size/4,  size/4, - size/4);
      //cara inferior
       glNormal3f(0,-1,0);
      
      glTexCoord2d(0, 0);glVertex3f(- size/4, - size/4,  size/4);
      glTexCoord2d(0, 1);glVertex3f( size/4, - size/4,  size/4);
      glTexCoord2d(1, 1);glVertex3f( size/4, - size/4, - size/4);
      glTexCoord2d(1, 0);glVertex3f(- size/4, - size/4, - size/4);
      
      glEnd();
  } 
}
