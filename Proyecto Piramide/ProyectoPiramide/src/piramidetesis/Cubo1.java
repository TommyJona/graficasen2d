/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramidetesis;

import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author alexo
 */
public class Cubo1 {
    float x,y,z;
    //dimensiones
    float w,h,d; //ancho, alto, profundidad
    //colores
    float c1,c2,c3;
    
    
    public Cubo1( float x, float y, float z, float w, float h, float d, float c1, float c2, float c3) {
      
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.d = d;
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;        
    }
    public void dibujarCubo(){
       
       
        
        float w1=w/2;
        float h1=h/2;
        float d1=d/2;
        
        //BASE
        glTranslatef(x, y, z);
        glBegin(GL_QUADS);
       
        glColor3f(c1,c2,c3);
        glVertex3f(0-w1, 0-h1, 0+d1);
        glVertex3f(0+w1, 0-h1, 0+d1);
        glVertex3f(0+w1, 0-h1, 0-d1);
        glVertex3f(0-w1, 0-h1, 0-d1);
        glEnd();
        
        //SUPERIOR
        glBegin(GL_QUADS);
        glColor3f(c1,c2,c3-0.2f);
        glVertex3f(0-w1, 0+h1, 0+d1);
        glVertex3f(0+w1, 0+h1, 0+d1);
        glVertex3f(0+w1, 0+h1, 0-d1);
        glVertex3f(0-w1, 0+h1, 0-d1);
        glEnd();
        
        //FRONTAL
        glBegin(GL_QUADS);
        glColor3f(c1,c2,c3);
        glVertex3f(0-w1, 0-h1, 0+d1);
        glVertex3f(0+w1, 0-h1, 0+d1);
        glVertex3f(0+w1, 0+h1, 0+d1);
        glVertex3f(0-w1, 0+h1, 0+d1);
        glEnd();
        
        //POSTERIOR
        glBegin(GL_QUADS);
        glColor3f(c1,c2,c3-0.2f);
        glVertex3f(0-w1, 0-h1, 0-d1);
        glVertex3f(0+w1, 0-h1, 0-d1);
        glVertex3f(0+w1, 0+h1, 0-d1);
        glVertex3f(0-w1, 0+h1, 0-d1);
        glEnd();
        
        //LATERAL IZQUIERDA
        glBegin(GL_QUADS);
        glColor3f(c1,c2-0.2f,c3-0.2f);
        glVertex3f(0-w1, 0-h1, 0+d1);
        glVertex3f(0-w1, 0-h1, 0-d1);
        glVertex3f(0-w1, 0+h1, 0-d1);
        glVertex3f(0-w1, 0+h1, 0+d1);
        glEnd();
        
        //LATERAL DERECHA
        glBegin(GL_QUADS);
        glColor3f(c1,c2-0.1f,c3-0.2f);
        glVertex3f(0+w1, 0-h1, 0+d1);
        glVertex3f(0+w1, 0-h1, 0-d1);
        glVertex3f(0+w1, 0+h1, 0-d1);
        glVertex3f(0+w1, 0+h1, 0+d1);
        glEnd();
        
    }
    public void trapecio(float r){
      
               
    
        float w1=w/2;
        float h1=h/2;
        float d1=d/2;
        
        //BASE
        glTranslatef(x, y, z);
        glBegin(GL_QUADS);
       
        glColor3f(c1,c2,c3);
        glVertex3f(0-w1, 0-h1, 0+d1);
        glVertex3f(0+w1, 0-h1, 0+d1);
        glVertex3f(0+w1, 0-h1, 0-d1);
        glVertex3f(0-w1, 0-h1, 0-d1);
        glEnd();
        
        //SUPERIOR
        glBegin(GL_QUADS);
        glColor3f(c1,c2,c3-0.2f);
        glVertex3f(0-w1+r, 0+h1, 0+d1);
        glVertex3f(0+w1-r, 0+h1, 0+d1);
        glVertex3f(0+w1-r, 0+h1, 0-d1);
        glVertex3f(0-w1+r, 0+h1, 0-d1);
        glEnd();
        
        //FRONTAL
        glBegin(GL_QUADS);
        glColor3f(c1,c2,c3);
        glVertex3f(0-w1, 0-h1, 0+d1);
        glVertex3f(0+w1, 0-h1, 0+d1);
        glVertex3f(0+w1-r, 0+h1, 0+d1);
        glVertex3f(0-w1+r, 0+h1, 0+d1);
        glEnd();
        
        //POSTERIOR
        glBegin(GL_QUADS);
        glColor3f(c1,c2,c3-0.2f);
        glVertex3f(0-w1, 0-h1, 0-d1);
        glVertex3f(0+w1, 0-h1, 0-d1);
        glVertex3f(0+w1-r, 0+h1, 0-d1);
        glVertex3f(0-w1+r, 0+h1, 0-d1);
        glEnd();
        
        //LATERAL IZQUIERDA
        glBegin(GL_QUADS);
        glColor3f(c1,c2-0.2f,c3-0.2f);
        glVertex3f(0-w1, 0-h1, 0+d1);
        glVertex3f(0-w1, 0-h1, 0-d1);
        glVertex3f(0-w1+r, 0+h1, 0-d1);
        glVertex3f(0-w1+r, 0+h1, 0+d1);
        glEnd();
        
        //LATERAL DERECHA
        glBegin(GL_QUADS);
        glColor3f(c1,c2-0.1f,c3-0.2f);
        glVertex3f(0+w1, 0-h1, 0+d1);
        glVertex3f(0+w1, 0-h1, 0-d1);
        glVertex3f(0+w1-r, 0+h1, 0-d1);
        glVertex3f(0+w1-r, 0+h1, 0+d1);
        glEnd();
    }
}
