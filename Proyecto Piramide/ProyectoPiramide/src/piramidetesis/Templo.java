/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramidetesis;

import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author alexo
 */
public class Templo {
    Escenarios esc=new Escenarios();
    String [][]matriz=esc.escenarioCinco();
    String [][]matriz1=esc.escenarioTres();
    Cubo1 cubase,cubolater,cubfront,cubosup,cubotecho;
    Palmeras pal;
    CasayPilares esfinge;
    Cilindro cil;
    public Templo(){
        cubase=new Cubo1(0, 0, 0, 2.3f, 8, 2.3f, 1, 1, 0);
        cubolater=new Cubo1(0, 0, 0, 2.3f, 8, 2.3f, 1, 1, 0);
        cubfront=new Cubo1(0,0,0,20,13,1,1,1,1);
        pal=new Palmeras(0, 0, 0, 1, 1, 1, 1, 1, 1);
        esfinge=new CasayPilares(0, 0, 0, 1, 1, 1, 1, 1, 0);
        //templo
        cubosup=new Cubo1(0, 0, 0, 1, 1, 1, 1, 1, 1);
        //pilares interiores
        cil=new Cilindro(0, 0, 0, 1, 0, 0, 1, 1, 4, 30, 30);
        //techo
        cubotecho=new Cubo1(0, 0, 0, 1, 1, 1, 1, 1,1);
    }
    public void dibujartemplo(){
        //paredes
       glPushMatrix();
       dibujarparedes();
       glPopMatrix();
       
       glPushMatrix();
       glTranslatef(0, 0, 16.08f);
       glScalef(1,1, 0.5f);
       dibujarparedesla();
       glPopMatrix();
       
       glPushMatrix();
       glTranslatef(0, 2.5f, 0);
       dibujarparedesfront();
       glPopMatrix();
       //palmeras
       glPushMatrix();
       glTranslatef(0, 1, -8);
       glScalef(3, 1, 1);
       dibujarparedespalmeras();
       glPopMatrix();
       //entrada
       glPushMatrix();
       glTranslatef(35, 1, -10);
       glScalef(4, 1, 0.5f);
       dibujaresfnentra();
       glPopMatrix();
       
       glPushMatrix();
       glTranslatef(10, 1, 8);
       glScalef(4, 1, 0.5f);
       glRotatef(180, 0, 1, 0);
       dibujaresfnentra();
       glPopMatrix();
       //parte superior
       glPushMatrix();
       glTranslatef(10, 9,3);
       glScalef(18, 1, 1.5f);
       glRotatef(90, 0, 1, 0);
       cubosup.dibujarCubo();
       glPopMatrix();
       
       glPushMatrix();
       glTranslatef(36, 9,3);
       glScalef(18, 1, 1.5f);
       glRotatef(90, 0, 1, 0);
       cubosup.dibujarCubo();
       glPopMatrix();
       
       glPushMatrix();
       glTranslatef(25, 5.5f,3);
       glScalef(22, 2.7f, 0.9f);
       glRotatef(90, 0, 1, 0);
       cubosup.dibujarCubo();
       glPopMatrix();
       //dibujar pilares interiores
       glPushMatrix();
       glTranslatef(3, 6, 4);
       glScalef(1.5f, 2.5f, 0.3f);
       dibujarpilaresint();
       glPopMatrix();
       
       glPushMatrix();
       glTranslatef(40, 6, 4);
       glScalef(1.5f, 2.5f, 0.3f);
       dibujarpilaresint();
       glPopMatrix();
       //dinujar techo
       glPushMatrix();
        glTranslatef(-1.5f,6, 4.8f);
        glScalef(2.5f, 1, 2.61f);
       dibujartecho();
       glPopMatrix();
    }
    public void dibujarparedes(){
        for (int x = 0; x <matriz.length; x++) {
                for (int z = 0; z < matriz[x].length; z++) {
                   glTranslated(x,0,z);
                    if (matriz[x][z].equals("2")) {
                        
                       cubase.dibujarCubo();
                    }
                    glTranslated(-x,0,-z);
               }
           }
    }
     public void dibujarparedesla(){
        for (int x = 0; x <matriz.length; x++) {
                for (int z = 0; z < matriz[x].length; z++) {
                   glTranslated(x,0,z);
                    if (matriz[x][z].equals("1")) {
                        
                       cubolater.dibujarCubo();
                    }
                    glTranslated(-x,0,-z);
               }
           }
    }
     
     public void dibujarparedesfront(){
        for (int x = 0; x <matriz.length; x++) {
                for (int z = 0; z < matriz[x].length; z++) {
                   glTranslated(x,0,z);
                    if (matriz[x][z].equals("3")) {
                        
                       cubfront.trapecio(4.5f);
                    }
                    glTranslated(-x,0,-z);
               }
           }
    }
     public void dibujaresfnentra(){
         glRotatef(-90, 0, 1, 0);
        for (int x = 0; x <matriz.length; x++) {
                for (int z = 0; z < matriz[x].length; z++) {
                   glTranslated(x,0,z);
                    if (matriz[x][z].equals("5")) {
                        
                       esfinge.dibujaresfinge();
                    }
                    glTranslated(-x,0,-z);
               }
           }
    }
     public void dibujarparedespalmeras(){
        for (int x = 0; x <matriz.length; x++) {
                for (int z = 0; z < matriz[x].length; z++) {
                   glTranslated(x,0,z);
                    if (matriz[x][z].equals("4")) {
                        
                       pal.dibujarPalmeras();
                    }
                    glTranslated(-x,0,-z);
               }
           }
    }
     public void dibujarpilaresint(){
         glRotatef(90, 1, 0, 0);
        for (int x = 0; x <matriz1.length; x++) {
                for (int z = 0; z < matriz1[x].length; z++) {
                   glTranslated(x,z,0);
                    if (matriz1[x][z].equals("6")) {
                        
                       cil.dibujarcilindro();
                    }
                    glTranslated(-x,-z,0);
               }
           }
    }
     public void dibujartecho(){
       
        for (int x = 0; x <matriz1.length; x++) {
                for (int z = 0; z < matriz1[x].length; z++) {
                   glTranslated(x,0,z);
                    if (matriz1[x][z].equals("7")) {
                        
                       cubotecho.dibujarCubo();
                    }
                    glTranslated(-x,0,-z);
               }
           }
    }
}
