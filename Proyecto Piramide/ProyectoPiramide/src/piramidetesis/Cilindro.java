/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramidetesis;

import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.util.glu.Cylinder;

/**
 *
 * @author alexo
 */
public class Cilindro {
    Cylinder cilindro;
    float baseradius,topradius,h,x,y,z,c1,c2,c3,pi;
    int slices,stacks;
    public Cilindro(float x,float y,float z,float c1,float c2,float c3,float baseradius,float topadius,float h,int slices, int stacks){
        this.x=x;
        this.y=y;
        this.z=z;
        this.c1=c1;
        this.c2=c2;
        this.c3=c3;
        this.baseradius=baseradius;
        this.topradius=topadius;
        this.h=h;
        this.slices=slices;
        this.stacks=stacks;
    }
    public void dibujarcilindro(){
        glPushMatrix();
         glTranslated(x, y, z);
         glColor3f(c1,c2, c3);
         cilindro=new Cylinder();
         cilindro.draw(baseradius, topradius, h, slices, stacks);

         dibujarcir();
         dibujarsemicir();
         glPopMatrix();
         
    }
    public void dibujarcir(){
        glTranslatef(x, y, z);
        glColor3f(c1,c2, c3);
         glBegin(GL_POLYGON);

            for(int i=0; i<100; i++){
            
               float x = ((float) Math.cos((i)*2*3.14/100))*baseradius;
               float y= (float) Math.sin((i)*2*3.14/100)*baseradius;
               glVertex2f(x, y); 
            }
            glEnd();
    }
    public void dibujarsemicir(){
        glTranslatef(x, y, h);
        glColor3f(c1,c2, c3);
         glBegin(GL_POLYGON);
         
            
            for(int i=0; i<100; i++){
            
               float x = ((float) Math.cos((i)*2*3.14/100))*topradius;
               float y= (float) Math.sin((i)*2*3.14/100)*topradius;
               glVertex2f(x, y); 
            }
            glEnd();
    }
}
