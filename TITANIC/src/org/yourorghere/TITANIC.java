package org.yourorghere;

import com.sun.corba.se.impl.logging.NamingSystemException;
import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;



/**
 * TITANIC.java <BR>
 * author: Brian Paul (converted to Java by Ron Cemer and Sven Goethel) <P>
 *
 * This version is equal to Brian Paul's version 1.2 1999/10/21
 */
public class TITANIC extends JFrame implements KeyListener {

Barco barco;
Barco2 barco2;
Barcoroto barcorot1;
HieloMuer hielo1;
Cubo cub;
public static GL gl;
public static GLUT glut;
    public static GLU glu;
    static float alto;
     
       public TITANIC() {
        setSize(1000, 800);
        setLocation(0, 0);
        setTitle("TITANIC");
        GraphicListener listener = new GraphicListener();
        GLCanvas canvas = new GLCanvas(new GLCapabilities());
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);        
        Animator animator = new Animator(canvas);
        animator.start();        
        addKeyListener(this);
        
        
    }
       public static void main(String args[]) {
        TITANIC myframe = new TITANIC();
        myframe.setVisible(true);
        myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
public  static float trasn=0;
public  static float trasn1=0;
public  static float trasn2=0;
public  static float trasn3=0;
public  static float trasn4=0;
public static float rotatex=0;
public static float rotatey=0;
public static float rotatez=0;
int[] lista = new int[100];
   public class GraphicListener implements GLEventListener {

        public void display(GLAutoDrawable arg0) {
           
             glu = new GLU();
             glut=new GLUT();
            
             gl = arg0.getGL();
            

            gl.glClear(GL.GL_COLOR_BUFFER_BIT|GL.GL_DEPTH_BUFFER_BIT);
            
            gl.glClearColor(0.0f, 0.5f, 0.5f, 0.0f);            
            
            gl.glMatrixMode(GL.GL_PROJECTION);
            
            gl.glLoadIdentity();
            barco=new Barco(gl);
            
            hielo1=new HieloMuer(gl);
            barco2=new Barco2(gl,glut);
            barcorot1=new Barcoroto(gl, glut);
            
            
            
            
            //creacion del cuadrado
           
//            gl.glPushMatrix();
//            gl.glScalef(0.1f, 0.1f, 0);
//             gl.glTranslatef(trasn2,trasn1, 0);
//            
//              
////               if (trasn2!=5) {
////            for (int i = 0; i <5 ; i++) {
////                        
////                    trasn2=trasn2+(i/0.01f);
////                }
////                
////            }
//            if (trasn2<=5) {
//                trasn2=(trasn2+0.01f);
//               barco.dinujarBarco();
//                
//            } 
//            if (trasn2>=5) {
//                 barco.dinujarBarco();
//                if (trasn1>=-4) {
//                      trasn1-=0.01f;
//                    
//                }
//                       
//                     }
              gl.glPushMatrix();
              gl.glTranslatef(0.5f, 0, 0);
              hielo1.dibujarhielomuer();
              gl.glPopMatrix();             
//PARA VER SOLUCIONADO movimiento :PRIMERO LA ESCALA LUEGO ROTACION Y LUEGOTRASLACION 
             gl.glPushMatrix();
             gl.glScalef(0.13f, 0.13f, 0.13f);
             gl.glTranslatef(trasn3-4, trasn4-2.1f, 0);
//             gl.glRotatef(rotatex, 1, 0, 0);
//             gl.glRotatef(rotatey, 0, 1, 0);
//             gl.glRotatef(rotatez, 0, 0, 1);
//             barco2.dibujarbarco2();
             if (trasn3<6) {
                trasn3=(trasn3+0.005f);
               
             barco2.dibujarbarco2();
             }
             if (trasn3>=6) {
                 barcorot1.dibujarbarcoroto();
                if (trasn4>=-7) {
                      trasn4-=0.005f;
                    
                }
                       
                     }
             gl.glPopMatrix();
            
             //trasn3+=0.002f;
             gl.glFlush();          
        }

        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }

        //En este metodo se queda el mismo segmento que hemos venido manejando
        public void init(GLAutoDrawable arg0) {
            GL gl = arg0.getGL();
            gl.glEnable(GL.GL_BLEND);
            gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
            gl.glEnable(GL.GL_DEPTH_TEST);//  Habilitar la prueba de profundidad de Z-buffer
        
        }

        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        }
    }

//    public void display(GLAutoDrawable drawable) {
//        GL gl = drawable.getGL();
//
//        // Clear the drawing area
//        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
//        // Reset the current matrix to the "identity"
//        gl.glLoadIdentity();
//        
//        
//      gl.glTranslatef(trasn, -1.5f, 0);
//        barco=new Barco(gl);
//       barco.dinujarBarco();
//        
//    
//        gl.glFlush();
//  
//    }
   
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
            trasn+=.01f;
            System.out.println("Valor en la traslacion de X: " + trasn);
        }
        if(e.getKeyCode() == KeyEvent.VK_LEFT){
            trasn-=.01f;
            System.out.println("Valor en la traslacion de X: " + trasn);
        }
         if(e.getKeyCode() == KeyEvent.VK_W){
            rotatex+=1f;
            System.out.println("Valor en la traslacion de X: " + rotatex);
        }
        if(e.getKeyCode() == KeyEvent.VK_S){
            rotatex-=1f;
            System.out.println("Valor en la traslacion de X: " + rotatex);
        }
         if(e.getKeyCode() == KeyEvent.VK_A){
            rotatey+=1f;
            System.out.println("Valor en la traslacion de X: " + rotatey);
        }
        if(e.getKeyCode() == KeyEvent.VK_D){
            rotatey-=1f;
            System.out.println("Valor en la traslacion de X: " + rotatey);
        }
         if(e.getKeyCode() == KeyEvent.VK_Q){
            rotatez+=1f;
            System.out.println("Valor en la traslacion de X: " + rotatez);
        }
        if(e.getKeyCode() == KeyEvent.VK_E){
            rotatez-=1f;
            System.out.println("Valor en la traslacion de X: " + rotatez);
        }

         if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
            trasn=0;
            
            
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    
}

