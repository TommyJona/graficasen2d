/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yourorghere;

import javax.media.opengl.GL;


/**
 *
 * @author alexo
 */
public class Cuadrado {
     GL gl;
    //coordenadas 
    float x,y;
    
    float a,h;
    float c1,c2,c3;
    public  Cuadrado(GL gl,float x,float y,float a,float h,float c1,float c2,float c3){
        this.gl=gl;
        this.x=x;
        this.y=y;
        this.a=a;
        this.h=h;
        this.x=x;
        this.c1=c1;
        this.c2=c2;
        this.c3=c3;
    }
    
    public void dibujarcu(){
        gl.glTranslated(x, y, 0);
        float a1=a/2;
        float h1=h/2;
        gl.glBegin(GL.GL_QUADS);
                     gl.glColor3f(c1,c2,c3); 
                     gl.glVertex3f(0-a1, 0-h1, 0.0f);  // Top Left
                     gl.glVertex3f(0+a1, 0-h1, 0.0f);   // Top Right
                     gl.glVertex3f(0+a1, 0+h1, 0.0f);  // Bottom Right
                     gl.glVertex3f(0-a1, 0+h1, 0.0f); // Bottom Left
                     gl.glEnd();  
    }
    public void dibujarTrapecio(float r){
               
     gl.glTranslatef(x,y,0);
        float w1=a/2;
        float h1=h/2;
        
        
        
        //FRONTAL
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(0-w1, 0-h1, 0);
        gl.glVertex3f(0+w1, 0-h1, 0);
        gl.glVertex3f(0+w1-r, 0+h1, 0);
        gl.glVertex3f(0-w1+r, 0+h1, 0);
        gl.glEnd();
        
    }        
}
