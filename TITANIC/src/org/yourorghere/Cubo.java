/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yourorghere;

import javax.media.opengl.GL;

/**
 *
 * @author alexo
 */
public class Cubo {
      GL gl;
    //coordenadas
    float x,y,z;
    //dimensiones
    float w,h,d; //ancho, alto, profundidad
    //colores
    float c1,c2,c3;
    
    
    public Cubo(GL gl, float x, float y, float z, float w, float h, float d, float c1, float c2, float c3) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.d = d;
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;        
    }

    
    
    
    public void dibujarCubo(){
       
       
     gl.glTranslatef(x,y,z);
        float w1=w/2;
        float h1=h/2;
        float d1=d/2;
        
       //BASE
        
        gl.glBegin(gl.GL_QUADS);
       
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(-w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, -d1);
        gl.glVertex3f(-w1, -h1, -d1);
        gl.glEnd();
        
        //SUPERIOR
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1+0.85f,c2+0.85f,c3+0.85f);
        gl.glVertex3f(-w1, +h1, +d1);
        gl.glVertex3f(+w1, +h1, +d1);
        gl.glVertex3f(+w1, +h1, -d1);
        gl.glVertex3f(-w1, +h1, -d1);
        gl.glEnd();
        
        //FRONTAL
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(-w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, +d1);
        gl.glVertex3f(+w1, +h1, +d1);
        gl.glVertex3f(-w1, +h1, +d1);
        gl.glEnd();
        
        //POSTERIOR
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(-w1, -h1, -d1);
        gl.glVertex3f(+w1, -h1, -d1);
        gl.glVertex3f(+w1, +h1, -d1);
        gl.glVertex3f(-w1, +h1, -d1);
        gl.glEnd();
        
        //LATERAL IZQUIERDA
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(-w1, -h1, +d1);
        gl.glVertex3f(-w1, -h1, -d1);
        gl.glVertex3f(-w1, +h1, -d1);
        gl.glVertex3f(-w1, +h1, +d1);
        gl.glEnd();
        
        //LATERAL DERECHA
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(+w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, -d1);
        gl.glVertex3f(+w1, +h1, -d1);
        gl.glVertex3f(+w1, +h1, +d1);
        gl.glEnd();
       
        
    }    
      public void dibujarCubo2(){
       
       
     gl.glTranslatef(x,y,z);
        float w1=w/2;
        float h1=h/2;
        float d1=d/2;
        
        //BASE
        
        gl.glBegin(gl.GL_QUADS);
       
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(-w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, -d1);
        gl.glVertex3f(-w1, -h1, -d1);
        gl.glEnd();
        
        //SUPERIOR
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(-w1, +h1, +d1);
        gl.glVertex3f(+w1, +h1, +d1);
        gl.glVertex3f(+w1, +h1, -d1);
        gl.glVertex3f(-w1, +h1, -d1);
        gl.glEnd();
        
        //FRONTAL
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(-w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, +d1);
        gl.glVertex3f(+w1, +h1, +d1);
        gl.glVertex3f(-w1, +h1, +d1);
        gl.glEnd();
        
        //POSTERIOR
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(-w1, -h1, -d1);
        gl.glVertex3f(+w1, -h1, -d1);
        gl.glVertex3f(+w1, +h1, -d1);
        gl.glVertex3f(-w1, +h1, -d1);
        gl.glEnd();
        
        //LATERAL IZQUIERDA
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(-w1, -h1, +d1);
        gl.glVertex3f(-w1, -h1, -d1);
        gl.glVertex3f(-w1, +h1, -d1);
        gl.glVertex3f(-w1, +h1, +d1);
        gl.glEnd();
        
        //LATERAL DERECHA
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(+w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, -d1);
        gl.glVertex3f(+w1, +h1, -d1);
        gl.glVertex3f(+w1, +h1, +d1);
        gl.glEnd();
        
    }    
    
    public void dibujarTrapecio(float r){
               
     gl.glTranslatef(x,y,z);
        float w1=w/2;
        float h1=h/2;
        float d1=d/2;
        
        //BASE
        
        gl.glBegin(gl.GL_QUADS);
       
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(-w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, -d1);
        gl.glVertex3f(-w1, -h1, -d1);
        gl.glEnd();
        
        //SUPERIOR
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1-.05f,c2-.05f,c3-.05f);
        gl.glVertex3f(-w1+r, +h1, +d1);
        gl.glVertex3f(+w1-r, +h1, +d1);
        gl.glVertex3f(+w1-r, +h1, -d1);
        gl.glVertex3f(-w1+r, +h1, -d1);
        gl.glEnd();
        
        //FRONTAL
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3);
        gl.glVertex3f(-w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, +d1);
        gl.glVertex3f(+w1-r, +h1, +d1);
        gl.glVertex3f(-w1+r, +h1, +d1);
        gl.glEnd();
        
        //POSTERIOR
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2,c3-.2f);
        gl.glVertex3f(-w1, -h1, -d1);
        gl.glVertex3f(+w1, -h1, -d1);
        gl.glVertex3f(+w1-r, +h1, -d1);
        gl.glVertex3f(-w1+r, +h1, -d1);
        gl.glEnd();
        
        //LATERAL IZQUIERDA
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2-.2f,c3-.2f);
        gl.glVertex3f(-w1, -h1, +d1);
        gl.glVertex3f(-w1, -h1, -d1);
        gl.glVertex3f(-w1+r, +h1, -d1);
        gl.glVertex3f(-w1+r, +h1, +d1);
        gl.glEnd();
        
        //LATERAL DERECHA
        gl.glBegin(gl.GL_QUADS);
        gl.glColor3f(c1,c2-.1f,c3-.2f);
        gl.glVertex3f(+w1, -h1, +d1);
        gl.glVertex3f(+w1, -h1, -d1);
        gl.glVertex3f(+w1-r, +h1, -d1);
        gl.glVertex3f(+w1-r, +h1, +d1);
        gl.glEnd();
    }
}
