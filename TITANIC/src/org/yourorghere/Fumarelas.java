/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yourorghere;


import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import com.sun.opengl.util.GLUT;

/**
 *
 * @author alexo
 */
public class Fumarelas {
        GLUT glut;
        GL gl;
    float x, y, z;
    float w, h, d;    
    float c1, c2, c3;
    int slices, stacks;
    double rs, ri;

    public Fumarelas(GL gl,GLUT glut, float x, float y, float z, float w, float h, float d, float c1, float c2, float c3, int slices, int stacks, double rs, double ri) {
        this.gl = gl;
        this.glut = glut;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.d = d;
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
        this.slices = slices;
        this.stacks = stacks;
        this.rs = rs;
        this.ri = ri;
    }
    
    public void dibujarFumarelas(){
        GLUT gluT = new GLUT();
        gl.glPushMatrix();
        gl.glColor3f(c1, c2, c3);
        gl.glScalef(w,h,d);
        glut.glutSolidCylinder(rs, ri, slices, stacks);
        gl.glPopMatrix();
        gl.glEnd();
    }
}
