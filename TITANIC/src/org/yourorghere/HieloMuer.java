/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yourorghere;

import javax.media.opengl.GL;

/**
 *
 * @author alexo
 */
public class HieloMuer {
    Iceberg ice1,ice2;
    GL gl;
    public HieloMuer(GL gl){
        this.gl=gl;
        //grupo iceberg que mato a Jack Dawson 
        ice1=new Iceberg(gl, 0, 0, 0, 1, 1.5f, 1, 1, 1, 1);
        ice2=new Iceberg(gl, 0, 0, 0, 1, 1.5f, 1, 1, 1, 1);
    }
    public void dibujarhielomuer(){
        
       gl.glPushMatrix();
       dibujarseriedetri();
       gl.glPopMatrix();
       
       gl.glPushMatrix();
       gl.glScalef(0.5f, 0.5f, 0.5f);
       gl.glTranslatef(0.7f, 0.5f, 1f);
       gl.glRotatef(20, -1, 1, 0);
       ice1.dibujariceberg();
       gl.glPopMatrix();
       
       gl.glPushMatrix();
       gl.glScalef(0.9f, 0.9f, 0.9f);
       gl.glTranslatef(0.5f, -0.2f, 0.9f);
       gl.glRotatef(20, -1, 1, 0);
       ice1.dibujariceberg();
       gl.glPopMatrix();
       
       gl.glPushMatrix();
       gl.glScalef(0.9f, 0.9f, 0.9f);
       gl.glTranslatef(0.2f, -0.2f, 0.9f);
       gl.glRotatef(30, -1, 1, 0);
       ice1.dibujariceberg();
       gl.glPopMatrix();
       
       gl.glPushMatrix();
       gl.glScalef(0.6f, 0.6f, 0.6f);
       gl.glTranslatef(-0.1f, -0.8f, 0.7f);
       gl.glRotatef(30, -1, 1, 0);
       ice2.dibujariceberg2();
       gl.glPopMatrix();
       
       gl.glPushMatrix();
       gl.glScalef(0.6f, 0.6f, 0.6f);
       gl.glTranslatef(0.2f, -0.8f, 0.7f);
       gl.glRotatef(30, -1, 1, 0);
       ice2.dibujariceberg2();
       gl.glPopMatrix();
       
       gl.glPushMatrix();
       gl.glScalef(0.6f, 0.6f, 0.6f);
       gl.glTranslatef(0.4f, -0.8f, 0.1f);
       gl.glRotatef(30, -1, 1, 0);
       ice2.dibujariceberg2();
       gl.glPopMatrix();
       
       gl.glPushMatrix();
       gl.glScalef(0.6f, 0.6f, 0.6f);
       gl.glTranslatef(0.8f, -0.88f, 0.1f);
       gl.glRotatef(30, -1, 1, 0);
       ice2.dibujariceberg2();
       gl.glPopMatrix();
       
    }
    public void dibujarseriedetri(){
        for (float i = 0; i < 2; i++) {
            for (float j = 0; j < 2; j++) {
                gl.glPushMatrix();
             gl.glScalef(0.3f, 0.3f, 0.3f);
             gl.glTranslatef(0.5f+j, 0.5f-i, 2);
              gl.glRotatef(20, -1, 1, 0);
        
        ice1.dibujariceberg();
        gl.glPopMatrix();
            }
            
        }
    }
    
}
