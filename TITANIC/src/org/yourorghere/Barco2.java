/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yourorghere;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

/**
 *
 * @author alexo
 */
public class Barco2 {
    GL gl;
    GLUT glut;
    Cubo cubo1,cubo2,cubo3,cubo4,cubo5,cubo6;
    Fumarelas fumarela1,fumarelas2;
    
    public Barco2(GL gl,GLUT glut){
        this.gl=gl;
         this.glut=glut;
        cubo1=new Cubo(gl, 0, 0, 0, 5, 1, 1, 0, 0, 0);
        cubo2=new Cubo(gl, 0, 0, 0, 0.5f, 0.5f, 10.5f, 1, 1, 1);
        cubo3=new Cubo(gl, 0, 0, 0, 4, 0.5f, 1, 1, 1, 1);
        cubo4=new Cubo(gl, 0, 0, 0, 0.3f, 0.3f, 10.5f, 0, 0, 0);
        cubo5=new Cubo(gl, 0, 0, 0, 1, 1.06f, 1.05f, 0, 0, 0);
        cubo6=new Cubo(gl, 0, 0, 0, 1, 1, 1, 0, 0, 1);
        //fumarelas
        fumarela1=new Fumarelas(gl, glut, 0, 0, 0, 0.2f, 0.2f,1f, 0, 0, 0, 30, 30, 1, 1);
        fumarelas2=new Fumarelas(gl, glut, 0, 0, 0, 0.2f, 0.2f, 0.5f, 0.81f, 0.71f, 0.23f, 30, 30, 1, 1);
        
    }
    public void dibujarbarco2(){
        gl.glPushMatrix();
        gl.glTranslatef(-0.5f, -0.7f, 0);
        gl.glRotatef(20, -1, 1, 0);
        cubo1.dibujarCubo();
        gl.glPopMatrix();
        //ventanas 
        gl.glPushMatrix();
        gl.glTranslatef(-0.5f, -0.7f, 0);
        
        gl.glRotatef(20, -1, 1, 0);
        dibujarventanas();
        gl.glPopMatrix();
        //trapecio superior
        gl.glPushMatrix();
        gl.glTranslatef(-0.5f, -0.7f, 0);
        
        gl.glRotatef(20, -1, 1, 0);
        gl.glTranslatef(-0.3f, 0.75f, 0);
        cubo3.dibujarTrapecio(2/8.2f);
        gl.glPopMatrix();
        //dibujar ventanas del trapecio
        gl.glPushMatrix();
        gl.glTranslatef(-0.5f, -0.7f, 0);
        gl.glRotatef(20, -1, 1, 0);
        gl.glTranslatef(0.3f, 0.85f, 0);
        
        dibujarventanas2();
        gl.glPopMatrix();
        //parte frontal barco
        gl.glPushMatrix();
        gl.glTranslatef(-0.5f, -0.7f, 0);
        gl.glRotatef(20, -1, 1, 0);
        gl.glTranslatef(2.4f, 0f, 0);
        gl.glRotatef(200, 0, 0, 0);
        cubo5.dibujarTrapecio(2/6.2f);
        gl.glPopMatrix();
       
        //dibujar fumarelas ocurre un error porque son cilindros libreria glut 
       //fumarela 2
       dibujarfumarelasama();
       
         dibujarfumarelas();  
    }
    public void dibujarventanas(){
         for (float i = 0; i < 23; i++) {
            for (float j = 0; j < 4; j++) {
                 gl.glPushMatrix();
                 
                gl.glTranslatef(-2.2f+i/5, -0.3f+j/6, 0);
                gl.glScalef(0.1f, 0.1f, 0.1f);
                cubo2.dibujarCubo2();
              gl.glPopMatrix();
            }
 
        }
    }
    public void dibujarventanas2(){
         for (float i = 0; i < 17; i++) {
            for (float j = 0; j < 3; j++) {
                 gl.glPushMatrix();
                 
                gl.glTranslatef(-2.2f+i/5, -0.3f+j/6, 0);
                gl.glScalef(0.1f, 0.1f, 0.1f);
                cubo4.dibujarCubo2();
              gl.glPopMatrix();
            }
 
        }
    }
    public void dibujarfumarelas(){
        for (float i = 0; i < 4; i++) {
             gl.glPushMatrix();
             gl.glRotatef(20, -1, 1, 0);
             gl.glTranslatef(-2.2f+i/1.1f, 1.1f, -0.25f);
             gl.glRotatef(90, 1, 0, 0);
                gl.glScalef(0.8f, 0.8f, 0.8f);
                fumarela1.dibujarFumarelas();
              gl.glPopMatrix();
        }
    }
    public void dibujarfumarelasama(){
        for (float i = 0; i < 4; i++) {
             gl.glPushMatrix();
             gl.glRotatef(20, -1, 1, 0);
             gl.glTranslatef(-2.2f+i/1.1f, 0.75f, -0.25f);
             gl.glRotatef(90, 1, 0, 0);
             gl.glScalef(0.85f, 0.85f, 0.85f);
             fumarelas2.dibujarFumarelas();
              gl.glPopMatrix();
        }
    }
    
}
