/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yourorghere;

import com.sun.org.apache.bcel.internal.generic.AALOAD;
import javax.media.opengl.GL;

/**
 *
 * @author alexo
 */
public class Barco {
    GL gl;
    Cuadrado cuadrado1,cuadrado2,cuadrado3,cuadrado4,cuadrado5;
    Triangulo triangulo;
    public  Barco(GL gl){
    this.gl=gl;
    cuadrado1=new Cuadrado(gl, 0, 0, 5.9f, 1f, 0, 0, 0);
    cuadrado2=new Cuadrado(gl, 0, 0, 0.05f, 0.05f, 1, 1, 1);
    cuadrado3=new Cuadrado(gl, 0, 0, 6.3f, 0.1f, 1, 1, 1);
    triangulo=new Triangulo(gl, 0, 0, 0.4f, 0.47f, 0, 0, 0);
    cuadrado4=new Cuadrado(gl, 0, 0, 5.3f, 0.4f, 0.9f, 0.9f, 0.9f);
    cuadrado5=new Cuadrado(gl, 0, 0, 0.04f, 0.04f, 1, 1, 1);
    }
    public void dinujarBarco(){
        //ventanas
         gl.glPushMatrix();
            
        dibujarventanas();
        gl.glPopMatrix();
        //parte superior blanca
        gl.glPushMatrix();
        gl.glTranslatef(-0.3f, 0.5f, 0);
        cuadrado3.dibujarcu();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(-0.5f, 0f, 0);
        cuadrado1.dibujarcu();
        gl.glPopMatrix();
         //superior trapecio ventanas
        gl.glPushMatrix();
        
        dibujarventanas1();
        gl.glPopMatrix();
        //frente barco
        gl.glPushMatrix();
        gl.glTranslatef(2.45f, -0.02f,0 );
        gl.glRotatef(180, 0, 0, 0);
        triangulo.dibujartriangulo();
        gl.glPopMatrix();
        //superior trapecio blanco
        gl.glPushMatrix();
        gl.glTranslatef(-0.6f, 0.75f, 0);
        cuadrado4.dibujarTrapecio((float)0.2);
        gl.glPopMatrix();
       
    }
    public void dibujarventanas(){
        
        for (float i = 0; i < 16; i++) {
            for (float j = 0; j < 4; j++) {
                 gl.glPushMatrix();
                gl.glTranslatef(-3f+i/3, -0.3f+j/6, 0);
                cuadrado2.dibujarcu();
              gl.glPopMatrix();
            }
 
        }
       
    }
    public void dibujarventanas1(){
        for (float i = 0; i < 28; i++) {
            for (float j = 0; j < 3; j++) {
                gl.glPushMatrix();
                gl.glTranslatef(-2.8f+i/6, 0.65f+j/9, 0);
                cuadrado5.dibujarcu();
              gl.glPopMatrix();
            }
            
        }
    }
}
