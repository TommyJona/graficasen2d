/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yourorghere;

import javax.media.opengl.GL;

/**
 *
 * @author alexo
 */
public class Triangulo {
    float x,y,c1,c2,c3,a,h;
    GL gl;
    
    public Triangulo(GL gl,float x,float y,float a,float h,float c1,float c2,float c3){
        this.gl=gl;
        this.x=x;
        this.y=y;
        this.a=a;
        this.h=h;
        this.c1=c1;
        this.c2=c2;
        this.c3=c3;
  
    }
    public void dibujartriangulo(){
     
            gl.glTranslatef(x, y, 0);
            float x1=a;
            float y1=h;
            gl.glBegin(GL.GL_TRIANGLES);
        
            gl.glColor3f(c1, c2, c3);
            gl.glVertex3f(0.0f, 0+y1, 0.0f);   
            
            gl.glVertex3f(0-x1, 0-y1, 0.0f); 
            
            gl.glVertex3f(0+x1, 0-y1, 0.0f);  
        
        gl.glEnd();
    }
}
