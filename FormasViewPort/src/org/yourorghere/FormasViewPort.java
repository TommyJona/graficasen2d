package org.yourorghere;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import javax.swing.JFrame;



/**
 * FormasViewPort.java <BR>
 * author: Brian Paul (converted to Java by Ron Cemer and Sven Goethel) <P>
 *
 * This version is equal to Brian Paul's version 1.2 1999/10/21
 */
public class FormasViewPort extends JFrame implements KeyListener {
 public static GL gl;
    public static GLU glu;
    public static GLUT glut;
    public static GLCanvas canvas;
    public static int ancho,alto;
    private static float rotarX=0;
    private static float rotarY=0;
    private static float rotarZ=0;
    private static float trasladaX=0;
    private static float trasladaY=0;
    private static float trasladaZ=0;
    private static float scaleX=0;
    private static float scaleY=0;
    private static float scaleZ=0;

     public static void main (String args[]){
         FormasViewPort myframe = new FormasViewPort();
         myframe.setVisible(true);
         myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

     public FormasViewPort(){
        setSize(700,600);
        setLocationRelativeTo(null);
        setTitle("Figuras con viewPort");
        setResizable(false);
        GraphicListener listener = new GraphicListener();
        alto = this.getHeight();
        ancho = this.getWidth();
        canvas= new GLCanvas();
        gl=canvas.getGL();
        glu= new GLU();
        glut = new GLUT();
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);
        Animator animator = new Animator(canvas);
        animator.start();
        addKeyListener(this);

     }

     public class GraphicListener implements GLEventListener{

        public void display(GLAutoDrawable arg0){
            gl=arg0.getGL();
            gl.glClear(GL.GL_COLOR_BUFFER_BIT);
                        gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); 
            gl.glMatrixMode(GL.GL_PROJECTION);
            gl.glLoadIdentity();
            

            
           //cubo
            gl.glViewport(0,456, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireCube(7.5f);
            
            gl.glViewport(140,456, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidCube(7.5f);
            //esfera
            gl.glViewport(280,456, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireSphere(5, 20, 20);
            
            gl.glViewport(420,456, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidSphere(5, 20, 20);
            //Cono
            gl.glViewport(560,456, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireCone(5, 5, 20, 20);
            
            gl.glViewport(0,342, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidCone(5, 5, 20, 20);
            //Cilndro
            gl.glViewport(140,342, ancho/5,(alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireCylinder(5, 5, 20, 20);
            
            gl.glViewport(280,342, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidCylinder(5, 5, 20, 20);
            //cilindro GLUquadric
            gl.glViewport(420,342, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            GLUquadric quad;
            quad=glu.gluNewQuadric();
            glu.gluQuadricDrawStyle(quad, GLU.GLU_FILL);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glu.gluCylinder(quad, 5, 5, 5, 20, 20);
            //dodecahedro
            gl.glViewport(560,342, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX*2.5f, scaleY*2.5f, scaleZ*2.5f);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireDodecahedron();
            
            gl.glViewport(0,228, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX*2.5f, scaleY*2.5f, scaleZ*2.5f);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidDodecahedron();
            //isocahedro
            gl.glViewport(140,228, ancho/5,(alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX*4.5f, scaleY*4.5f, scaleZ*4.5f);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireIcosahedron();
            
            gl.glViewport(280,228, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX*4.5f, scaleY*4.5f, scaleZ*4.5f);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidIcosahedron();
             //rombododecahedro
            gl.glViewport(420,228, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX*4.5f, scaleY*4.5f, scaleZ*4.5f);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireRhombicDodecahedron();
            
            gl.glViewport(560,228, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX*4.5f, scaleY*4.5f, scaleZ*4.5f);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidRhombicDodecahedron();
             //dona
            gl.glViewport(0,114, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireTorus(1, 3, 20, 20);
            
            gl.glViewport(140,114, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidTorus(1, 3, 20, 20);
             //tetera
            gl.glViewport(280,114, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireTeapot(5);
            
            gl.glViewport(420,114, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidTeapot(5);
              //tetrahedro
            gl.glViewport(560,114, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX*5, scaleY*5, scaleZ*5);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireTetrahedron();
            
            gl.glViewport(0,0, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX*5, scaleY*5, scaleZ*5);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidTetrahedron();
              //octahedro
            gl.glViewport(140,0, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX*4.5f, scaleY*4.5f, scaleZ*4.5f);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireOctahedron();
            
            gl.glViewport(280,0, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX*4.5f, scaleY*4.5f, scaleZ*4.5f);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidOctahedron();
               //disco
            gl.glViewport(420,0, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutWireTorus(1, 5, 2, 20);
            
            gl.glViewport(560,0, ancho/5, (alto/5)-6);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0, trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 0);
            glut.glutSolidTorus(1, 5, 2, 20);
             
             
//            gl.glViewport(350, (alto/2), ancho/2, alto/2);
//            gl.glLoadIdentity();
//            
//            gl.glOrtho(-5, 5, -5, 5, -5, 5);
//            gl.glTranslatef(trasladaX, 0, 0);
//            gl.glTranslatef(0, trasladaY, 0);
//            gl.glTranslatef(0, 0, trasladaZ);
//            gl.glScalef(scaleX, scaleY, scaleZ);
//            
//            gl.glRotatef(rotarX, 1f, 0f, 0f);
//            gl.glRotatef(rotarY, 0f, 1f, 0f);
//            gl.glRotatef(rotarZ, 0f, 0f, 1f);
//            gl.glColor3f(1, 1, 0);
//            glut.glutWireCone(2, 6, 20, 20);
//            
//            
//            gl.glViewport(0, (alto)-700, ancho, alto);
//            gl.glLoadIdentity();
//            gl.glOrtho(-5, 5, -5, 5, -5, 5);
//            gl.glTranslatef(-4, 0, 0);
//            gl.glTranslatef(0, trasladaY, 0);
//            gl.glTranslatef(0, 0, trasladaZ);
//            gl.glScalef(scaleX, scaleY, scaleZ);
//            gl.glRotatef(rotarX, 1f, 0f, 0f);
//            gl.glRotatef(rotarY, 0f, 1f, 0f);
//            gl.glRotatef(rotarZ, 0f, 0f, 1f);
//            gl.glColor3f(1, 1, 1);
//            glut.glutWireCone(2, 6, 20, 20);
//
//           
            gl.glFlush();
        }

         public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2){
        }

        public void init(GLAutoDrawable arg0){         
        }

        public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4){
        }//reshape...
     }//graphic listener


     //Creacion del metodo que detecta las teclas pulsadas
    public void keyPressed(KeyEvent arg0){
        
        if(arg0.getKeyCode()==KeyEvent.VK_RIGHT){
            trasladaX+=.10f;
            System.out.println("El valor de Z: "+trasladaX);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_LEFT){
            trasladaX-=.10f;
            System.out.println("El valor de Z: "+trasladaX);
        }
           if(arg0.getKeyCode()==KeyEvent.VK_UP){
            trasladaY+=.10f;
            System.out.println("El valor de Z: "+trasladaY);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_DOWN){
            trasladaY-=.10f;
            System.out.println("El valor de Z: "+trasladaY);
        }
           if(arg0.getKeyCode()==KeyEvent.VK_V){
            trasladaZ+=.10f;
            System.out.println("El valor de Z: "+trasladaZ);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_B){
            trasladaZ-=.10f;
            System.out.println("El valor de Z: "+trasladaZ);
        }
          if(arg0.getKeyCode()==KeyEvent.VK_A){
            rotarX+=1.0f;
            System.out.println("El valor de X en la rotacion: "+rotarX);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_D){
            rotarX-=1.0f;
            System.out.println("El valor de X en la rotacion: "+rotarX);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_W){
            rotarY+=1.0f;
            System.out.println("El valor de Y en la rotacion: "+rotarY);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_S){
            rotarY-=1.0f;
            System.out.println("El valor de Y en la rotacion: "+rotarY);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_Q){
            rotarZ+=1.0f;
            System.out.println("El valor de Z en la rotacion: "+rotarZ);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_R){
            rotarZ-=1.0f;
            System.out.println("El valor de Z en la rotacion: "+rotarZ);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_J){
            scaleX+=.10f;
            System.out.println("El valor de Z: "+scaleX);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_L){
            scaleX-=.10f;
            System.out.println("El valor de Z: "+scaleX);
        }
           if(arg0.getKeyCode()==KeyEvent.VK_I){
            scaleY+=.10f;
            System.out.println("El valor de Z: "+scaleY);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_K){
            scaleY-=.10f;
            System.out.println("El valor de Z: "+scaleY);
        }
           if(arg0.getKeyCode()==KeyEvent.VK_U){
            scaleZ+=.10f;
            System.out.println("El valor de Z: "+scaleX);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_O){
            scaleZ-=.10f;
            System.out.println("El valor de Z: "+scaleX);
        } 

          if(arg0.getKeyCode()==KeyEvent.VK_ESCAPE){
            rotarX=0;
            rotarY=0;
            rotarZ=0;
            trasladaX=0;
            trasladaY=0;
            trasladaZ=0;
            scaleX=0;
            scaleY=0;
            scaleZ=0;
        }
    }//fin keyPressed

     public void keyReleased(KeyEvent arg0){}
     public void keyTyped(KeyEvent arg0){}}


