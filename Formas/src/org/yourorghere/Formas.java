package org.yourorghere;

import com.sun.opengl.util.Animator;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import com.sun.opengl.util.GLUT;
//librerias para teclas
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.sun.opengl.util.Animator;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.glu.GLUquadric;

public class Formas  extends JFrame implements KeyListener {

   
    public static GLU glu;
    public static GLUT glut;
    public Formas() {
        setSize(900, 1000);
        setLocation(1, 1);
        setTitle("Uso Basico de Tranformaciones");
        GraphicListener listener = new GraphicListener();
        GLCanvas canvas = new GLCanvas(new GLCapabilities());
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);        
        Animator animator = new Animator(canvas);
        animator.start();        
        addKeyListener(this);
    }

    public static void main(String args[]) {
        Formas myframe = new Formas();
        myframe.setVisible(true);
        myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

     private static float traslacionX=0;
    private static float traslacionY=0;
    private static float escalaX=0;
    private static float escalaY=1f;
    private static float rotacionX=1f;
    private static float rotacionY=0;
    private static float rotacionZ=0;
    private static float ang=0;
     private static float tras=-0.75f;
     private static float trasy=0.75f;

    public class GraphicListener implements GLEventListener {

        public void display(GLAutoDrawable drawable) {
           GL gl= drawable.getGL();
            glu = new GLU();
            glut=new GLUT();
            
            

            gl.glClear(GL.GL_COLOR_BUFFER_BIT|GL.GL_DEPTH_BUFFER_BIT);
            
//            gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);            
//            
//            gl.glMatrixMode(GL.GL_PROJECTION);
//            gl.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
//            //el grosor de las lineas
//            gl.glLineWidth(2f);
//            //el tama�o de los puntos
//            gl.glPointSize(3.0f);
            //Matriz identidad
            gl.glLoadIdentity();

            //creacion del cuadrado
//            gl.glTranslatef(traslacionX, traslacionY, 0);
//            gl.glScalef(escalaX, escalaY, 0);
//            gl.glRotatef(rotacionX, 1, 0, 0);
//            gl.glRotatef(rotacionY, 0, 1, 0);
//            gl.glRotatef(rotacionZ, 0, 0, 1);
//            gl.glColor3f(1, 1, 1);//se le da color
//            gl.glBegin(gl.GL_QUADS);//se crea el cuadrado
//                gl.glVertex2f(0, 0);
//                gl.glVertex2f(0.5f, 0);
//                gl.glVertex2f(0.5f, 0.5f);
//                gl.glVertex2f(0, 0.5f);
//            gl.glEnd();
            //Cubo
            gl.glPushMatrix();
            gl.glColor3f(0.0f, 1.0f, 0.0f);
            gl.glTranslatef(-0.8f,0.75f,0f);
            gl.glRotatef(ang,-1,0,1);
            glut.glutWireCube(0.15f);
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0.0f, 1.0f, 0.0f);
            gl.glTranslatef(-0.5f,0.75f,0f);
            gl.glRotatef(ang,-1,0,-1);
            glut.glutSolidCube(0.15f);
            gl.glPopMatrix();
            //esfera
            gl.glPushMatrix();
            gl.glColor3f(0.0f, 1.0f, 1.0f);
            gl.glTranslatef(-0.2f,0.75f,0f);
            gl.glRotatef(ang,0,1,1);
            glut.glutWireSphere(0.15f, 20, 20);
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0.0f, 1.0f, 1.0f);
            gl.glTranslatef(0.2f,0.75f,0f);
            gl.glRotatef(ang,0,1,-1);
            glut.glutSolidSphere(0.15f, 12, 12);    
            gl.glPopMatrix();
            //cono
            gl.glPushMatrix();
            gl.glColor3f(0.0f, 1.0f, 0.5f);
            gl.glTranslatef(0.5f,0.75f,0f);
            gl.glRotatef(ang,1,1,1);
            gl.glScalef(5,5,1);
            glut.glutWireCone(0.1f, 0.2f, 12,12); 
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0.0f, 1.0f, 0.5f);
            gl.glTranslatef(0.85f,0.75f,0f);
            gl.glRotatef(ang,1,1,-1);
            glut.glutSolidCone(0.1f, 0.2f, 12,12);    
            gl.glPopMatrix();
            //cilindro
            gl.glPushMatrix();
            gl.glColor3f(0.0f, 0.5f, 1.0f);
            gl.glTranslatef(-0.85f,0.4f,0f);
            gl.glRotatef(ang,1,0,1);
            glut.glutWireCylinder(0.1f, 0.15f,15,15);;    
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0.0f, 0.5f, 1.0f);
            gl.glTranslatef(-0.47f,0.4f,0f);
            gl.glRotatef(ang,1,1,-1);
            glut.glutSolidCylinder(0.1f, 0.15f,15,15);;    
            gl.glPopMatrix();
            
            
           //cilindro GLUquadric
            GLUquadric quad;
            quad=glu.gluNewQuadric();
            glu.gluQuadricDrawStyle(quad, GLU.GLU_FILL);
             gl.glPushMatrix();
            gl.glColor3f(1.0f, 1.0f, 1.0f);
            gl.glTranslatef(-0.15f,0.4f,0f);
            gl.glRotatef(ang,1,1,1);
            glu.gluCylinder(quad, 0.1, 0.1, 0.15f, 15, 15);    
            gl.glPopMatrix();
            //dodecahedro
             gl.glPushMatrix();
            gl.glColor3f(0.0f, 1.0f, 0f);
            gl.glTranslatef(0.2f,0.4f,0f);
            gl.glRotatef(ang,0,1,-1);
            gl.glScalef(0.07f, 0.07f, 0.07f);
            glut.glutWireDodecahedron();
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0.0f, 1.0f, 0f);
            gl.glTranslatef(0.5f,0.4f,0f);
            gl.glRotatef(ang,0,1,1);
            gl.glScalef(0.07f, 0.07f, 0.07f);
            glut.glutSolidDodecahedron();
            gl.glPopMatrix();
            //isochedro
            gl.glPushMatrix();
            gl.glColor3f(0.5f, 1.0f, 1.0f);
            gl.glTranslatef(0.8f,0.4f,0f);
            gl.glRotatef(ang,1,1,-1);
            gl.glScalef(0.15f, 0.15f, 0.15f);
            glut.glutWireIcosahedron();
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0.5f, 1.0f, 1.0f);
            gl.glTranslatef(0.8f,0f,0f);
            gl.glRotatef(ang,1,-1,1);
            gl.glScalef(0.15f, 0.15f, 0.15f);
            glut.glutSolidIcosahedron();
            gl.glPopMatrix();
            //rombododecahedro
            gl.glPushMatrix();
            gl.glColor3f(0.5f, 0f, 1.5f);
            gl.glTranslatef(0.5f,0f,0f);
            gl.glRotatef(ang,-1,0,-1);
            gl.glScalef(0.15f, 0.15f, 0.15f);
            glut.glutWireRhombicDodecahedron();
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0.5f, 0f, 1.5f);
            gl.glTranslatef(0.18f,0f,0f);
            gl.glRotatef(ang,-1,0,1);
            gl.glScalef(0.15f, 0.15f, 0.15f);
            glut.glutSolidRhombicDodecahedron();
            gl.glPopMatrix();
            //dona
            gl.glPushMatrix();
            gl.glColor3f(0.5f, 0f, 0f);
            gl.glTranslatef(-0.13f,0f,0f);
            gl.glRotatef(ang,1,1,-1);
            gl.glScalef(0.15f, 0.15f, 0.15f);
            glut.glutWireTorus(0.35, 0.55, 15, 15);
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0.5f, 0f, 0f);
            gl.glTranslatef(-0.45f,0f,0f);
            gl.glRotatef(ang,1,-1,1);
            gl.glScalef(0.15f, 0.15f, 0.15f);
            glut.glutSolidTorus(0.35, 0.55, 15, 15);
            gl.glPopMatrix();
           //Tetera
           gl.glPushMatrix();
            gl.glColor3f(0.5f, 0.5f, 0f);
            gl.glTranslatef(-0.81f,0f,0f);
            gl.glRotatef(ang,1,0,-1);
            gl.glScalef(0.12f, 0.12f, 0.12f);
            glut.glutWireTeapot(1);
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0.5f, 0.5f, 0f);
            gl.glTranslatef(-0.81f,-0.4f,0f);
            gl.glRotatef(ang,1,1,1);
            gl.glScalef(0.12f, 0.12f, 0.12f);
            glut.glutSolidTeapot(1);
            gl.glPopMatrix();
            //Tetrahedro
            gl.glPushMatrix();
            gl.glColor3f(0.5f, 0.5f, 0.5f);
            gl.glTranslatef(-0.45f,-0.4f,0f);
            gl.glRotatef(ang,1,0,-1);
            gl.glScalef(0.17f, 0.17f, 0.17f);
            glut.glutWireTetrahedron();
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0.5f, 0.5f, 0.5f);
            gl.glTranslatef(-0.15f,-0.4f,0f);
            gl.glRotatef(ang,1,0,1);
            gl.glScalef(0.17f, 0.17f, 0.17f);
            glut.glutSolidTetrahedron();
            gl.glPopMatrix();
            ang+=0.1f;
            //octahedro
            gl.glPushMatrix();
            gl.glColor3f(0f, 0f, 1f);
            gl.glTranslatef(0.2f,-0.4f,0f);
            gl.glRotatef(ang,1,1,-1);
            gl.glScalef(0.17f, 0.17f, 0.17f);
            glut.glutWireOctahedron();
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0f, 0f, 1f);
            gl.glTranslatef(0.6f,-0.4f,0f);
            gl.glRotatef(ang,1,1,1);
            gl.glScalef(0.17f, 0.17f, 0.17f);
            glut.glutSolidOctahedron();
            gl.glPopMatrix();
            //disco
            gl.glPushMatrix();
            gl.glColor3f(0.5f, 1.5f, 0.5f);
            gl.glTranslatef(0.1f,-0.7f,0f);
            gl.glRotatef(ang,1,1,-1);
            gl.glScalef(0.2f, 0.2f, 0.2f);
            glut.glutWireTorus(0.2, 0.3, 2, 20);
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glColor3f(0.5f, 1.5f, 0.5f);
            gl.glTranslatef(0.6f,-0.7f,0f);
            gl.glRotatef(ang,1,1,1);
            gl.glScalef(0.2f, 0.2f, 0.2f);
            glut.glutSolidTorus(0.2, 0.3, 2, 20);
            gl.glPopMatrix();
            ang+=0.1f;
            
            gl.glFlush();
            
            
        }

        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }

        //En este metodo se queda el mismo segmento que hemos venido manejando
        public void init(GLAutoDrawable arg0) {
            GL gl = arg0.getGL();
            gl.glEnable(GL.GL_BLEND);
            gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        }

        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        }
    }

    //Metodo para el teclado
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
            traslacionX+=.1f;
            System.out.println("Valor en la traslacion de X: " + traslacionX);
        }

        if(e.getKeyCode() == KeyEvent.VK_LEFT){
            traslacionX-=.1f;
            System.out.println("Valor en la traslacion de X: " + traslacionX);
        }

        if(e.getKeyCode() == KeyEvent.VK_UP){
            traslacionY+=.1f;
            System.out.println("El valor en la traslacion de Y: " + traslacionY);
        }

        if(e.getKeyCode() == KeyEvent.VK_DOWN){
            traslacionY-=.1f;
            System.out.println("El valor en la traslacion de Y: " + traslacionY);
        }

        if(e.getKeyCode() == KeyEvent.VK_D){
            escalaX+=1f;
            System.out.println("El valor en la escalacion en X: " + escalaX);
        }

        if(e.getKeyCode() == KeyEvent.VK_A){
            escalaX-=1f;
            System.out.println("El valor en la escalacion en X: " + escalaX);
        }
        if(e.getKeyCode() == KeyEvent.VK_Q){
            escalaY+=1f;
            System.out.println("El valor en la escalacion en X: " + escalaY);
        }

        if(e.getKeyCode() == KeyEvent.VK_R){
            escalaY-=1f;
            System.out.println("El valor en la escalacion en X: " + escalaY);
        }

         if(e.getKeyCode() == KeyEvent.VK_L){
            rotacionX+=5f;
            System.out.println("El valor en la rotacion en X: " + rotacionX);
        }

        if(e.getKeyCode() == KeyEvent.VK_J){
            rotacionX-=5f;
            System.out.println("El valor en la rotacion en X: " + rotacionX);
        }

        if(e.getKeyCode() == KeyEvent.VK_I){
            rotacionY+=5f;
            System.out.println("El valor en la rotacion en Y: " + rotacionY);
        }

        if(e.getKeyCode() == KeyEvent.VK_K){
            rotacionY-=5f;
            System.out.println("El valor en la rotacion en Y: " + rotacionY);
        }
        if(e.getKeyCode() == KeyEvent.VK_O){
            rotacionZ+=5f;
            System.out.println("El valor en la rotacion en Y: " + rotacionZ);
        }

        if(e.getKeyCode() == KeyEvent.VK_U){
            rotacionZ-=5f;
            System.out.println("El valor en la rotacion en Y: " + rotacionZ);
        }
        

         if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
            traslacionX=0;
            traslacionY=0;
            escalaX=1f;
            escalaY=1f;
            rotacionX=0;
            rotacionY=0;
            rotacionZ=0;
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }
}

