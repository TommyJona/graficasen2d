package org.yourorghere;


import com.sun.opengl.util.Animator;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

import javax.swing.JFrame;
import com.sun.opengl.util.GLUT;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;



/**
 * Viewport.java <BR>
 * author: Brian Paul (converted to Java by Ron Cemer and Sven Goethel) <P>
 *
 * This version is equal to Brian Paul's version 1.2 1999/10/21
 */
public class Viewport extends JFrame implements KeyListener {
 
    public static GL gl;
    public static GLU glu;
    public static GLUT glut;
    public static GLCanvas canvas;
    public static int ancho,alto;
    private static float rotarX=0;
    private static float rotarY=0;
    private static float rotarZ=0;
    private static float trasladaX=0;
    private static float trasladaY=0;
    private static float trasladaZ=0;
    private static float scaleX=0;
    private static float scaleY=0;
    private static float scaleZ=0;

     public static void main (String args[]){
         Viewport myframe = new Viewport();
         myframe.setVisible(true);
         myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

     public Viewport(){
        setSize(700,600);
        setLocationRelativeTo(null);
        setTitle("Figuras con viewPort");
        setResizable(false);
        GraphicListener listener = new GraphicListener();
        alto = this.getHeight();
        ancho = this.getWidth();
        canvas= new GLCanvas();
        gl=canvas.getGL();
        glu= new GLU();
        glut = new GLUT();
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);
        Animator animator = new Animator(canvas);
        animator.start();
        addKeyListener(this);

     }

     public class GraphicListener implements GLEventListener{

        public void display(GLAutoDrawable arg0){
            gl=arg0.getGL();
            gl.glClear(GL.GL_COLOR_BUFFER_BIT);
            gl.glColor3f(1f, 1f, 1f);

            gl.glMatrixMode(GL.GL_PROJECTION);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);

            
            gl.glViewport(0, 380, ancho/3, alto/3);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(0, 0, 0);
            gl.glTranslatef(0, 0, 0);
            gl.glTranslatef(0, 0,0);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 1);
            glut.glutWireRhombicDodecahedron();
            
            gl.glViewport(0, 0, ancho, (int)(alto/1.57));
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glTranslatef(0, trasladaY, 0);
            gl.glTranslatef(0, 0,trasladaZ);
            gl.glScalef(scaleX, scaleY, scaleZ);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(1,0, 1);
            glut.glutSolidRhombicDodecahedron();
             
//            gl.glViewport(350, (alto/2), ancho/2, alto/2);
//            gl.glLoadIdentity();
//            gl.glOrtho(-5, 5, -5, 5, -5, 5);
//            gl.glTranslatef(trasladaX, 0, 0);
//            gl.glTranslatef(0, trasladaY, 0);
//            gl.glTranslatef(0, 0, trasladaZ);
//            gl.glScalef(scaleX, scaleY, scaleZ);
//            
//            gl.glRotatef(rotarX, 1f, 0f, 0f);
//            gl.glRotatef(rotarY, 0f, 1f, 0f);
//            gl.glRotatef(rotarZ, 0f, 0f, 1f);
//            gl.glColor3f(1, 1, 0);
//            glut.glutWireCone(2, 6, 20, 20);
//            
//            gl.glViewport(0, (alto)-700, ancho, alto);
//            gl.glLoadIdentity();
//            gl.glOrtho(-5, 5, -5, 5, -5, 5);
//            gl.glTranslatef(-4, 0, 0);
//            gl.glTranslatef(0, trasladaY, 0);
//            gl.glTranslatef(0, 0, trasladaZ);
//            gl.glScalef(scaleX, scaleY, scaleZ);
//            gl.glRotatef(rotarX, 1f, 0f, 0f);
//            gl.glRotatef(rotarY, 0f, 1f, 0f);
//            gl.glRotatef(rotarZ, 0f, 0f, 1f);
//            gl.glColor3f(1, 1, 1);
//            glut.glutWireCone(2, 6, 20, 20);

           
            gl.glFlush();
        }

         public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2){
        }

        public void init(GLAutoDrawable arg0){         
        }

        public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4){
        }//reshape...
     }//graphic listener


     //Creacion del metodo que detecta las teclas pulsadas
    public void keyPressed(KeyEvent arg0){
        
        if(arg0.getKeyCode()==KeyEvent.VK_RIGHT){
            trasladaX+=.10f;
            System.out.println("El valor de Z: "+trasladaX);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_LEFT){
            trasladaX-=.10f;
            System.out.println("El valor de Z: "+trasladaX);
        }
           if(arg0.getKeyCode()==KeyEvent.VK_UP){
            trasladaY+=.10f;
            System.out.println("El valor de Z: "+trasladaY);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_DOWN){
            trasladaY-=.10f;
            System.out.println("El valor de Z: "+trasladaY);
        }
           if(arg0.getKeyCode()==KeyEvent.VK_V){
            trasladaZ+=.10f;
            System.out.println("El valor de Z: "+trasladaZ);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_B){
            trasladaZ-=.10f;
            System.out.println("El valor de Z: "+trasladaZ);
        }
          if(arg0.getKeyCode()==KeyEvent.VK_A){
            rotarX+=1.0f;
            System.out.println("El valor de X en la rotacion: "+rotarX);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_D){
            rotarX-=1.0f;
            System.out.println("El valor de X en la rotacion: "+rotarX);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_W){
            rotarY+=1.0f;
            System.out.println("El valor de Y en la rotacion: "+rotarY);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_S){
            rotarY-=1.0f;
            System.out.println("El valor de Y en la rotacion: "+rotarY);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_Q){
            rotarZ+=1.0f;
            System.out.println("El valor de Z en la rotacion: "+rotarZ);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_R){
            rotarZ-=1.0f;
            System.out.println("El valor de Z en la rotacion: "+rotarZ);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_J){
            scaleX+=.10f;
            System.out.println("El valor de Z: "+scaleX);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_L){
            scaleX-=.10f;
            System.out.println("El valor de Z: "+scaleX);
        }
           if(arg0.getKeyCode()==KeyEvent.VK_I){
            scaleY+=.10f;
            System.out.println("El valor de Z: "+scaleY);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_K){
            scaleY-=.10f;
            System.out.println("El valor de Z: "+scaleY);
        }
           if(arg0.getKeyCode()==KeyEvent.VK_U){
            scaleZ+=.10f;
            System.out.println("El valor de Z: "+scaleX);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_O){
            scaleZ-=.10f;
            System.out.println("El valor de Z: "+scaleX);
        } 

          if(arg0.getKeyCode()==KeyEvent.VK_ESCAPE){
            rotarX=0;
            rotarY=0;
            rotarZ=0;
            trasladaX=0;
            trasladaY=0;
            trasladaZ=0;
            scaleX=0;
            scaleY=0;
            scaleZ=0;
        }
    }//fin keyPressed

     public void keyReleased(KeyEvent arg0){}
     public void keyTyped(KeyEvent arg0){}
}

