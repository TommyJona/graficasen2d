import numpy as np
import matplotlib.pyplot as plt
import random as rand

# Funciones

def turn_right():
    array = [coord_points[0], coord_points[1]]
    for i in range(2, len(coord_points)):
        array.append(coord_points[i])
        # np.linalg Son funciones de álgebra lineal de NumPy
        while len(array) > 2 and np.linalg.det([array[-3], array[-2], array[-1]]) > 0:
            array.pop(-2)
    return array

def convex_all():
    coord_points.sort()
    l_upper = turn_right()
    coord_points.reverse()
    l_lower = turn_right()
    l = l_upper + l_lower
    return l

def graph(convex_pol, coord_points):
    # Acomodando listas adecuadas para graFicar en matplot
    x_points = [i[0] for i in coord_points]
    y_points = [i[1] for i in coord_points]
    x_polygon = [i[0] for i in convex_pol]
    y_polygon = [i[1] for i in convex_pol]
    # Definiendo límites extremos de la gráfica
    x_lim_der = max(x_points) + 5
    y_lim_sup = max(y_points) + 5
    x_lim_izq = min(x_points) - 5
    y_lim_inf = min(y_points) - 5
    # Asignación de los límites extremos
    plt.xlim(x_lim_izq, x_lim_der)
    plt.ylim(y_lim_inf, y_lim_sup)
    # Graficación
    plt.title('Problema: Envolvente Convexa')
    plt.xlabel('Eje de las abscisas')
    plt.ylabel('Eje de las ordenadas')
    #grafica los puntos observables
    plt.plot(x_points, y_points, 'ro')
    plt.plot(x_polygon, y_polygon, 'green', linewidth = 2.0)
    plt.show()

# Generación de coordenadas de manera aleatoria (números reales)
num_points = 100
coord_points = []
for i in range(num_points):
    coord_points.append([rand.uniform(0, 100), rand.uniform(0, 200), 1.0])
# Creación y graficación del polígono convexo
convex_pol = convex_all()
graph(convex_pol, coord_points)

